import numpy as np
import matplotlib.pyplot as plt


def plot_unit_circle(p, samples):
    x = 3 * np.random.rand(samples, 2) - 1.5
    n = np.linalg.norm(x, p, axis=1)
    index_in = np.flatnonzero(n <= 1)
    index_out = np.flatnonzero(n > 1)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(x[index_out, 0], x[index_out, 1], c='red', s=60, alpha=0.1, marker='*')
    ax.scatter(x[index_in, 0], x[index_in, 1], c='black', s=60, marker='+')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.grid(True, linestyle='-', color='0.75')
    plt.show()


if __name__ == '__main__':
    for i in [1, 2, 5, np.inf]:
        plot_unit_circle(i, 5000)
