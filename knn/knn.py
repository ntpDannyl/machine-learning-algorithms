import numpy as np
from scipy.spatial import KDTree


class Knn:
    def __init__(self):
        self.x_min = 0
        self.x_max = 0
        self.x_train = []  # ndarray
        self.y_train = None
        self.kd_tree = None

    @staticmethod
    def knn_classification(x_train, y_train, x_test, k, norm_order):
        x_min = x_train.min(axis=0)
        x_max = x_train.max(axis=0)
        x_train = (x_train - x_min) / (x_max - x_min)
        x_test = (x_test - x_min) / (x_max - x_min)
        diff = x_train - x_test
        dist = np.linalg.norm(diff, axis=1, ord=norm_order)
        k_nearest = np.argpartition(dist, k)[0:k]
        classification, counts = np.unique(y_train[k_nearest], return_counts=True)
        return classification[np.argmax(counts)]

    def regression_fit(self, x, y):
        self.x_min = x.min(axis=0)
        self.x_max = x.max(axis=0)
        self.x_train = (x - self.x_min) / (self.x_max - self.x_min)
        self.kd_tree = KDTree(self.x_train)
        self.y_train = y

    def regression_predict(self, x, k=3, smear=1.0):
        x = (x - self.x_min) / (self.x_max - self.x_min)
        (dist, neighbours) = self.kd_tree.query(x, k)
        dist_sum = np.sum(1 / (dist + smear / k), axis=1)
        dist_sum = np.repeat(dist_sum[:, None], k, axis=1)
        dist = (1 / dist_sum) * 1 / (dist + smear / k)
        y = np.sum(dist * self.y_train[neighbours], axis=1)
        return y
