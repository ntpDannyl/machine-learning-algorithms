import numpy as np
import matplotlib.pyplot as plt
from pca import PCA
from trees.CART import CART
from basics.refactor import train_test_split

np.random.seed(42)
scale = False

if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = dataset[:, 0:4]
    y = dataset[:, 4]

    if scale:
        x[:, 0] = 100 * x[:, 0]

    x_train, x_test, y_train, y_test = train_test_split(x, y, 0.8)
    full_tree = CART(min_leaf_node_size=5)
    full_tree.fit(x_train, y_train)

    y_predict = full_tree.predict(x_test)
    diffs = y_predict - y_test
    print(f'Accuracy Full Tree: {len(diffs[diffs == 0]) / len(diffs):.2f}%')

    pca = PCA(x)
    x_projected, wp = pca.get_projection(2)
    pca.plot()
    x_train, x_test, y_train, y_test = train_test_split(x_projected, y, 0.8)

    reduced_tree = CART(min_leaf_node_size=5)
    reduced_tree.fit(x_train, y_train)
    y_predict = reduced_tree.predict(x_test)
    diffs = y_predict - y_test
    print(f'Accuracy Reduced Tree: {len(diffs[diffs == 0]) / len(diffs):.2f}%')
    # The accuracy is lower, but we also have 2 dimensions less and only 1 more error in absolute numbers.

    plt.figure()
    plt.scatter(x_projected[0:50, 0], x_projected[0:50, 1], c='red', s=60, alpha=0.6)
    plt.scatter(x_projected[50:100, 0], x_projected[50:100, 1], c='green', marker='^', s=60, alpha=0.6)
    plt.scatter(x_projected[100:150, 0], x_projected[100:150, 1], c='blue', marker='*', s=80, alpha=0.6)
    plt.xlabel('1. Principal Component')
    plt.ylabel('2. Principal Component')
    plt.grid(True, linestyle='-', color='0.75')
    plt.show()