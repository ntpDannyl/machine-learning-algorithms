import time
import matplotlib.pyplot as plt
import numpy as np
from mlp import Mlp


ETA = 0.75
MAX_ITERATIONS = 200
EPSILON = 10 ** -3
HIDDEN_LAYERS = [8, 4]  # 3, 4
normalize = True

if __name__ == '__main__':

    # create dataset: x1 and x2 is the flat position, y is the height of each point
    x_train = np.random.rand(5000, 2)
    y_train = np.sin(2 * np.pi * (x_train[:, 0] + 0.5 * x_train[:, 1])) + 0.5 * x_train[:, 1]

    # add noise. our mlp will be able to learn the function without the noise by generalizing
    noise = np.random.rand(y_train.shape[0]) - 0.5
    y_train = (1 + 0.05 * noise) * y_train

    # create data to test the mlp
    x_test = np.random.rand(5000, 2)
    y_test = np.sin(2 * np.pi * (x_test[:, 0] + 0.5 * x_test[:, 1])) + 0.5 * x_test[:, 1]

    # instantiate mlp with 3 and 4 neurons as hidden-layers
    mlp = Mlp(HIDDEN_LAYERS, normalize)
    # train and time it
    train_time = time.time()
    mlp.fit(x_train, y_train, ETA, MAX_ITERATIONS, EPSILON)
    train_time = time.time() - train_time

    # test and time it
    test_time = time.time()
    yp = np.squeeze(mlp.predict(x_test))
    test_time = time.time() - test_time

    # plot the real function
    fig = plt.figure(figsize=(14, 12))
    ax = fig.add_subplot(2, 2, 1, projection='3d')
    ax.scatter(x_test[:, 0], x_test[:, 1], y_test, alpha=0.6, c=yp)
    ax.set_xlabel('x[0]')
    ax.set_ylabel('x[1]')
    ax.set_zlabel('$y$')

    # plot the predicted function
    ax = fig.add_subplot(2, 2, 2, projection='3d')
    ax.scatter(x_test[:, 0], x_test[:, 1], yp, alpha=0.6, c=yp)
    ax.set_xlabel('x[0]')
    ax.set_ylabel('x[1]')
    ax.set_zlabel('$y_p$')

    # plot the difference
    ax = fig.add_subplot(2, 2, 3, projection='3d')
    ax.scatter(x_test[:, 0], x_test[:, 1], yp.T - y_test, alpha=0.6, c=yp.T - y_test)
    ax.set_xlabel('x[0]')
    ax.set_ylabel('x[1]')
    ax.set_zlabel('$y_p - y$')

    # plot the error over time
    ax = fig.add_subplot(2, 2, 4)
    epochs = np.arange(len(mlp.error))
    ax.plot(epochs, np.array(mlp.error), 'k')
    ax.set_xlabel('Epochs')
    ax.set_ylabel('Average error')
    fig.tight_layout(pad=5)
    # plt.savefig('myfirstmlp.png', dpi=1000)
    plt.show(dpi=1000)

    # and as always: some prints
    print(f'Last Average Error: {mlp.error[-1]}')
    print(f'Epochs needed: {len(mlp.error)}')
    print(f'Time needed for training: {train_time:.4f} seconds')
    print(f'Time needed for testing: {test_time:.4f} seconds')
