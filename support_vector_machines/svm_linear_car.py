import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from basics.refactor import normalize_x
C = 1000

if __name__ == '__main__':
    dataset = np.loadtxt('./../problems/Cars2Classes.csv', delimiter=",")
    y = dataset[:, 0]
    x = normalize_x(dataset[:, 1:3])

    svm_linear = svm.SVC(kernel='linear', C=C)
    svm_linear.fit(x, y)

    w = svm_linear.coef_[0]
    a = -w[0] / w[1]
    xx = np.linspace(-1, 1, 50)
    yy = a * xx - (svm_linear.intercept_[0]) / w[1]
    margin = 1 / np.sqrt(np.sum(svm_linear.coef_ ** 2))
    yMarginDown = yy - np.sqrt(1 + a ** 2) * margin
    yMarginUp = yy + np.sqrt(1 + a ** 2) * margin

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(xx, yy, 'k')
    plt.plot(xx, yMarginDown, 'k--')
    plt.plot(xx, yMarginUp, 'k--')
    iA = np.flatnonzero(y == 1)
    iB = np.flatnonzero(y == 0)
    ax.scatter(x[iA, 0], x[iA, 1], c='red', marker='+', linewidth=1.5)
    ax.scatter(x[iB, 0], x[iB, 1], c='black', marker='o', linewidth=1.5)
    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_ylim([-0.25, 1.25])
    ax.set_xlim([0, 1])
    ax.set_title(f'Calculated with C={C}')
    plt.show()
