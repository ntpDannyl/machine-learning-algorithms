import numpy as np
import matplotlib.pyplot as plt
from problems.two_moons_problem import two_moons_problem
from random_forest import RandomForest

np.random.seed(42)


if __name__ == '__main__':

    (x, y) = two_moons_problem()

    forest = RandomForest(trees=24, min_leaf_node_size=5, threshold=0.1)
    forest.fit(x, y)

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 1, 1)
    index_a = np.flatnonzero(y > 0.5)
    index_b = np.flatnonzero(y < 0.5)
    ax.scatter(x[index_a, 0], x[index_a, 1], color='red', marker='o')
    ax.scatter(x[index_b, 0], x[index_b, 1], color='black', marker='+')

    counter = 0
    x_predict = np.zeros((961, 2))
    for i in np.arange(-1, 2.1, 0.1):
        for j in np.arange(-1, 2.1, 0.1):
            x_predict[counter] = [i, j]
            counter += 1

    y_predict = np.round(forest.predict(x_predict))
    for i in range(counter):
        if y_predict[i] > 0.5:
            ax.scatter(x_predict[i, 0], x_predict[i, 1], color='red', marker='o')
        else:
            ax.scatter(x_predict[i, 0], x_predict[i, 1], color='black', marker='+')

    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_ylim([-1, 2])
    ax.set_ylim([-1, 2])
    ax.set_title("Random Forest on Two Moons Set")
    plt.show()
