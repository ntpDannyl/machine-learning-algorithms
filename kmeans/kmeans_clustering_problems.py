import numpy as np
import matplotlib.pyplot as plt
from problems.clustering_problems import four_balls, two_moons, two_circles, just_noise, mouse
from kmeans import KMeans

np.random.seed(42)
max_iterations = 42
norm_order = 2

if __name__ == '__main__':
    sets_and_clusters = [
        {
            'dataset': four_balls(400, 400, 400, 400),
            'n_clusters': 4
        },
        {
            'dataset': two_moons(240, 0),
            'n_clusters': 2
        },
        {
            'dataset': two_circles(800),
            'n_clusters': 2
        },
        {
            'dataset': mouse(150, 150, 700),
            'n_clusters': 4
        },
        {
            'dataset': just_noise(1000),
            'n_clusters': 2
        },
    ]

    for entry in sets_and_clusters:
        dataset = entry['dataset']
        n_clusters = entry['n_clusters']
        kmeans = KMeans(n_clusters, norm_order)
        _, cluster = kmeans.fit(dataset, max_iterations)

        fig = plt.figure(1)
        ax = fig.add_subplot(1, 1, 1)
        markers = ['*', 's', '<', 'o', 'X', '8', 'p', 'h', 'H', 'D', 'd', 'P']
        colorName = ['black', 'red', 'blue', 'green']
        for j in range(n_clusters):
            index = np.flatnonzero(cluster == j)
            ax.scatter(dataset[index, 0], dataset[index, 1], c=colorName[j], s=60, alpha=0.2, marker=markers[j])
        for i in range(len(kmeans.fitHistory)):
            for j in range(n_clusters):
                ax.text(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1], str(i), style='italic',
                        bbox={'facecolor': 'white', 'alpha': 0.7, 'pad': 2}, color=colorName[j])
                ax.annotate(str(i), xy=(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1]), color=colorName[j])
                # ax.scatter(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1], c='m', s=60, marker=markers[j])

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.grid(True, linestyle='-', color='0.75')
        ax.set_aspect('equal', 'datalim')
        plt.show()
