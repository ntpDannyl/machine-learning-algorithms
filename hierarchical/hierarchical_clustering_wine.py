from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from scipy.spatial.distance import pdist
import numpy as np
from basics.refactor import normalize_x

if __name__ == '__main__':
    dataset = np.loadtxt('./../problems/wine.csv', delimiter=",")

    x = dataset[:, 1:14]
    y = dataset[:, 0]

    D = pdist(normalize_x(x), metric='euclidean')
    Z = linkage(D, 'complete', metric='euclidean')
    xCluster = fcluster(Z, 3, criterion='maxclust')

    labels, label_start, label_count = np.unique(y, return_counts=True, return_index=True)
    group_samples = np.zeros(3)
    label_start = np.append(label_start, len(y))
    for i in range(0, 3):
        group_samples[i] = label_start[i + 1] - label_start[i]
    fp = 0
    for i in range(0, 3):
        group, count = np.unique(xCluster[label_start[i]:label_start[i + 1]], return_counts=True)
        countSortInd = np.argsort(-count)
        groupValue = group[countSortInd[0]]
        fp = fp + count[countSortInd[0]]
        print(f'Wine class {i + 1} (clustered as {groupValue}): {count[countSortInd[0]]}/{group_samples[i]:.0f} ({count[countSortInd[0]] / group_samples[i] * 100:.2f}%) correct.')
    print(f'\nOverall sucess: {(fp / len(y) * 100):.1f}%')
    fig = plt.figure()
    dendrogram(Z, truncate_mode='lastp', p=12)
    plt.show()
