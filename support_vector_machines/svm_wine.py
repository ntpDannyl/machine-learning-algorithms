from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix


def plot_first_ten(images):
    fig, axs = plt.subplots(2, 5)
    for i in range(10):
        array = images[i].reshape(28, 28)
        if i < 5:
            axs[0, i].imshow(array, cmap="gray")
        else:
            axs[1, i - 5].imshow(array, cmap="gray")
    plt.show()


if __name__ == '__main__':
    print('fetching...')
    x, y = fetch_openml('mnist_784', version=1, return_X_y=True, as_frame=False)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)
    for images in [x, x_train, x_test]:
        plot_first_ten(images)

    svm_classifier = svm.SVC()
    print('fitting...')
    svm_classifier.fit(x_train, y_train)
    print('predicting...')
    prediction = svm_classifier.predict(x_test)
    print(f"Accuracy - {accuracy_score(y_pred=prediction, y_true=y_test):.2f}")
    confusion = confusion_matrix(y_pred=prediction, y_true=y_test)
    print(confusion_matrix)
