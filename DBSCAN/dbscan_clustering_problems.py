import numpy as np
from problems.clustering_problems import four_balls, two_moons, two_circles, just_noise, mouse
from dbscan import DBSCAN

np.random.seed(42)
min_points = 5

if __name__ == '__main__':

    sets_and_clusters = [
        {
            'dataset': four_balls(400, 400, 400, 400),
            'epsilon': 0.4,
            'title': 'Four Balls'
        },
        {
            'dataset': four_balls(400, 400, 100, 100),
            'epsilon': 0.4,
            'title': 'Four Balls'
        },
        {
            'dataset': two_moons(240, 3),
            'epsilon': 0.1,
            'title': 'Two Moons'
        },
        {
            'dataset': two_circles(800),
            'epsilon': 0.4,
            'title': 'Circles'
        },
        {
            'dataset': mouse(150, 150, 700),
            'epsilon': 0.07,
            'title': 'Mouse Set'
        },
        {
            'dataset': just_noise(1000),
            'epsilon': 0.05,
            'title': 'Noise'
        },
    ]

    for entry in sets_and_clusters:
        x = entry['dataset']
        epsilon = entry['epsilon']
        dbscan = DBSCAN(x)
        _ = dbscan.analyse_epsilon(min_points)
        c = dbscan.fit_predict(epsilon, min_points)
        dbscan.plot(entry['title'])
