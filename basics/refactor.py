import numpy as np
import pandas as pd


def standardize_x(x):
    xbar = np.mean(x, axis=0)
    sigma = np.std(x, axis=0)
    return (x - xbar) / sigma


def normalize_x(x):  # normalize in interval [0, 1]
    x_max = np.max(x, axis=0)
    x_min = np.min(x, axis=0)
    return (x - x_min) / (x_max - x_min)


def normalize_x_plusminus_one(x):  # normalize in interval [-1, 1]
    return 2 * normalize_x(x) - 1


def train_test_split(x, y, percentage, using_pd=False):
    train_set = np.random.choice(x.shape[0], int(x.shape[0] * percentage), replace=False)
    test_set = np.delete(np.arange(0, len(y)), train_set)

    x_train = x.iloc[train_set, :] if using_pd else x[train_set, :]
    y_train = y.iloc[train_set] if using_pd else y[train_set]
    x_test = x.iloc[test_set, :] if using_pd else x[test_set, :]
    y_test = y.iloc[test_set] if using_pd else y[test_set]
    return x_train, x_test, y_train, y_test
