import numpy as np

np.random.seed(42)


class KMeans:
    def __init__(self, n_clusters, norm_order):
        self.n_clusters = n_clusters
        self.p = norm_order
        self.fitHistory = []
        self.centers = None

    def _compute_distances(self, x, centres):
        distances = (np.sum(np.abs(x - centres[0, :]) ** self.p, axis=1)) ** (1 / self.p)
        for j in range(1, self.n_clusters):
            distances_to_add = (np.sum(np.abs(x - centres[j, :]) ** self.p, axis=1)) ** (1 / self.p)
            distances = np.vstack((distances, distances_to_add))
        return distances

    def fit(self, x, max_iterations):
        x_min = x.min(axis=0)
        x_max = x.max(axis=0)
        new_centers = np.random.rand(self.n_clusters, x.shape[1]) * (x_max - x_min) + x_min
        old_centers = new_centers + 1
        count = 0
        cluster = 0
        while np.sum(np.abs(old_centers - new_centers)) != 0 and count < max_iterations:
            count += 1
            old_centers = np.copy(new_centers)
            self.fitHistory.append(new_centers.copy())
            distances = self._compute_distances(x, new_centers)
            cluster = distances.argmin(axis=0)

            for j in range(self.n_clusters):
                index = np.flatnonzero(cluster == j)
                if index.shape[0] > 0:
                    new_centers[j, :] = np.sum(x[index, :], axis=0) / index.shape[0]
                else:
                    distances = (np.sum((x - new_centers[j, :]) ** self.p, axis=1)) ** (1 / self.p)
                    i = distances.argmin(axis=0)
                    new_centers[j, :] = x[i, :]
                    cluster[i] = j
        self.centers = new_centers
        return new_centers, cluster

    def predict(self, x):
        distances = self._compute_distances(x, self.centers)
        return distances.argmin(axis=0)  # index of cluster