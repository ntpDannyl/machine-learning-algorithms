import numpy as np
import matplotlib.pyplot as plt
from trees.CART import CART


class RandomForest:
    def __init__(self, trees=10, threshold=10 ** -8, x_decimals=8, min_leaf_node_size=3, bagging_percentage=1, is_regression=False):
        self.n_trees = trees
        self.threshold = threshold
        self.x_decimals = x_decimals
        self.min_leaf_node_size = min_leaf_node_size
        self.perc = bagging_percentage
        self.trees = []
        self.samples = []
        self.n_features = 0
        for i in range(trees):
            self.trees.append(CART(self.threshold, self.x_decimals, self.min_leaf_node_size, is_regression, True, 0))

    def fit(self, x, y):
        self.n_features = x.shape[1]
        for i in range(self.n_trees):
            bootstrap_sample = np.random.randint(x.shape[0], size=int(self.perc * x.shape[0]))
            self.samples.append(bootstrap_sample)
            bootstrap_x = x[bootstrap_sample, :]
            bootstrap_y = y[bootstrap_sample]
            self.trees[i].fit(bootstrap_x, bootstrap_y)

    def predict(self, x):
        ypredict = np.zeros(x.shape[0])
        for i in range(self.n_trees):
            ypredict += self.trees[i].predict(x)
        ypredict = ypredict / self.n_trees
        return ypredict

    def plot_features(self):
        features = []
        for tree in self.trees:
            features.append(tree.tree.root_node.var_no)
        unique, counts = np.unique(features, return_counts=True)
        plt.bar(unique, counts)
        plt.xlim([0, self.n_features])
        plt.xlabel("Most Important Feature")
        plt.ylabel("Number of Trees")
        plt.show()


