# Machine Learning Algorithms
This repository contains every algorithm I used for my master's thesis. Most of the algorithms are implemented using only numpy.

## How does this repository work?
- Problems: This folder contains many CSV files to test the algorithms.
- Basics: This folder contains some basics for analyzing data (like plotting standard deviation) or changing it (like normalizing).
- Other Folders: These contain algorithms implemented by hand. If you want a CART algorithm, go to `./trees/CART.py`. If you want a DBSCAN algorithm, go to `./DBSCAN/DBSCAN.py`, and so on. The other files contain the algorithm paired with a specific problem.

## Sources
This repository heavily relies on the book _Maschinelles Lernen_ by _Jörg Frochte (3rd edition, 2020)_, as well as many different articles from various sources. Most of the time, you can find the source used in a comment in the first lines of a file.

## Free to use
Enjoy!
