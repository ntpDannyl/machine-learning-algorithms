# based on: https://www.ted.com/talks/hans_rosling_the_best_stats_you_ve_ever_seen

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from dbscan import DBSCAN

# we have our own plot method here because we want them sweet annotations.
markers = ['.', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', 'P', 'h', 'H', '+', 'x',
           'X', 'd', 'D', '|', '_']
colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan',
          'lightcoral', 'firebrick', 'chocolate', 'saddlebrown', 'sandybrown', 'burlywood', 'gold',
          'yellowgreen', 'yellow', 'lime', 'aqua', 'dodgerblue', 'darkmagenta', 'slateblue']


def plot(title, dataset, clusters, feature_x=0, feature_y=1, xlim=None, ylim=None):
    try:
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        index = np.flatnonzero(clusters == -1)
        ax.scatter(dataset[index, feature_x], dataset[index, feature_y], c='black', s=30, marker='*')

        for i in range(clusters.shape[0]):
            ax.annotate(iso3c94.iloc[i], (dataset[i, feature_x], dataset[i, feature_y]), size='x-small', alpha=0.5)
        for i in range(0, clusters.max() + 1):
            index = np.flatnonzero(clusters == i)
            ax.scatter(dataset[index, feature_x], dataset[index, feature_y], c=colors[i], s=60, alpha=0.9, marker=markers[i])

        if xlim is not None:
            ax.set_xlim(xlim[0], xlim[1])
        if ylim is not None:
            ax.set_ylim(ylim[0], ylim[1])
        ax.set_title(title)
        plt.show()
    except IndexError:
        print('Too many clusters to print. Consider re-analyzing epsilon.')


if __name__ == '__main__':
    df = pd.read_csv('./../problems/nations.csv')

    # save labels for plotting and then drop them from dataframe
    all_iso3c = df.iso3c.copy()
    df = df.drop(columns='iso3c')

    # save countries and regions for controlling and ID them
    countries = df.country.unique()
    regions = df.region.unique()
    df['country'] = pd.factorize(df.country)[0]
    df['region'] = pd.factorize(df.region)[0]

    # replace strings in 'income' with mean value of that group. See Frochte 273.
    df['income'].replace(to_replace=['Low income'], value=(995 - 300) / 2, inplace=True)
    df['income'].replace(to_replace=['Lower middle income'], value=(3895 - 996) / 2, inplace=True)
    df['income'].replace(to_replace=['Upper middle income'], value=(12055 - 3896) / 2, inplace=True)
    df['income'].replace(to_replace=['High income'], value=(42380 - 12056) / 2, inplace=True)
    df['income'].replace(to_replace=['High income: OECD'], value=42380, inplace=True)

    # impute NaN with mean value of the region in the same year: This whole thing could be better I suppose.
    for feature in range(3, 9):
        for year in range(df['year'].min(), df['year'].max() + 1):
            data = df.values[np.flatnonzero(df['year'] == year), :]
            for region in range(df['region'].min(), df['region'].max() + 1):
                year_and_region = data[np.flatnonzero(data[:, 1] == region), :]
                mean = np.nanmean(year_and_region[:, feature])
                b_year_and_region = np.logical_and(df['year'] == year, df['region'] == region)
                b_nan = np.isnan(df.values[:, feature])
                nan_indices = np.flatnonzero(np.logical_and(b_nan, b_year_and_region))
                # this line is bad programming, it should be fixed with df.loc somehow
                df[df.keys()[feature]][nan_indices] = mean

    # save processed dataset
    df.to_csv('./../problems/nationsCodiert.csv', sep=',', index=False)

    # for this experiment we only use 2 years
    indices1994 = (df['year'] == 1994)
    indices2014 = (df['year'] == 2014)

    data_pca = df[np.logical_or(indices1994, indices2014)].values
    iso3c94 = all_iso3c[indices1994]
    data1994 = df[indices1994].values
    iso3c14 = all_iso3c[indices2014]
    data2014 = df[indices2014].values

    # standardize
    features = data_pca[:, [3, 4, 6, 7, 8]]
    xbar = np.mean(features, axis=0)
    sigma = np.std(features, axis=0)
    x = (features - xbar) / sigma

    # Do PCA and get WP
    (lamb, W) = np.linalg.eig(np.cov(x.T))
    eigenVar = np.sort(lamb)[::-1] / np.sum(lamb)
    cumVar = np.cumsum(eigenVar)
    eigenVarIndex = np.argsort(lamb)[::-1]
    WP = W[:, eigenVarIndex[0:2]]

    # 1994
    x = (data1994[:, [3, 4, 6, 7, 8]] - xbar) / sigma
    x_proj = (WP.T @ x.T).T

    scan = DBSCAN(x_proj)
    _ = scan.analyse_epsilon(4)
    c = scan.fit_predict(0.3, 4)

    plot('Countries in 1994', x_proj, c, 0, 1, [-4, 6], [-2, 2])
    plot('Countries in 1994 (newborn-rate / GDP)', data1994, c, 6, 3, [0, 70], [0, 80000])
    plot('Countries in 1994 (life-expectancy / GDP)', data1994, c, 4, 3)
    print('Noise: ', len(np.flatnonzero(c == -1)) / c.shape[0])

    # 2014
    x = (data2014[:, [3, 4, 6, 7, 8]] - xbar) / sigma
    x_proj = (WP.T @ x.T).T

    scan = DBSCAN(x_proj)
    _ = scan.analyse_epsilon(4)
    c = scan.fit_predict(0.32, 4)

    plot('Countries in 2014', x_proj, c, 0, 1, [-4, 6], [-2, 2])
    plot('Countries in 2014 (newborn-rate / GDP)', data2014, c, 6, 3, [0, 70], [0, 80000])
    plot('Countries in 2014 (life-expectancy / GDP)', data2014, c, 4, 3)  # [0, 70], [0, 80000]

    print('Noise: ', len(np.flatnonzero(c == -1)) / c.shape[0])
