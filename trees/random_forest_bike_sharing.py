import numpy as np
from random_forest import RandomForest
import matplotlib.pyplot as plt

np.random.seed(42)

# This script compares different sizes of a random forest and a reduced feature space

if __name__ == '__main__':
    y_diff_plot = []
    ydiff_plot_reduced = []
    forest_sizes = np.arange(5, 21, 5)

    dataset = np.loadtxt("./../problems/bike_sharing_dataset.csv", delimiter=",", skiprows=1)
    x = dataset[:, 0:13]
    y = dataset[:, 15]

    index = np.flatnonzero(x[:, 8] == 4)  # delete weathersit == 4 (snowing), because we only got 3 entries for that
    x = np.delete(x, index, axis=0)
    y = np.delete(y, index, axis=0)

    # splitting training- and testset, but not randomly.
    # We take the first 20 days of a month for training, the rest for testing.
    training_set = np.flatnonzero(x[:, 3] < 21)
    test_set = np.delete(np.arange(0, x.shape[0]), training_set)

    x_train = x[training_set, :]
    x_test = x[test_set, :]
    x_train = np.delete(x_train, 3, axis=1)
    x_test = np.delete(x_test, 3, axis=1)

    y_train = y[training_set]
    y_test = y[test_set]

    # Delete aTemperature, windspeed and season
    x_reduced = np.delete(x, [0, 10, 12], axis=1)

    x_train_reduced = x_reduced[training_set, :]
    x_test_reduced = x_reduced[test_set, :]
    x_train_reduced = np.delete(x_train_reduced, 2, axis=1)
    x_test_reduced = np.delete(x_test_reduced, 2, axis=1)

    y_train_reduced = y[training_set]
    y_test_reduced = y[test_set]

    for forest_size in forest_sizes:
        print(f'Experiment with {forest_size} trees.')
        # With all features
        forest = RandomForest(trees=forest_size, threshold=10 ** -2, x_decimals=3, is_regression=True)
        forest.fit(x_train, y_train)
        y_predict = np.round(forest.predict(x_test))
        y_diff = np.abs(y_predict - y_test)
        y_diff_mean = np.mean(y_diff)
        print('  Full set:')
        print(f'    Average Deviance: {y_diff_mean}')
        print(f'    Maximal Deviance: {np.max(y_diff)}')
        forest.plot_features()

        # Reduced Featureset without aTemperature, Windspeed und Season
        forest = RandomForest(trees=forest_size, threshold=2, x_decimals=3, is_regression=True)
        forest.fit(x_train_reduced, y_train_reduced)
        y_predict_reduced = np.round(forest.predict(x_test_reduced))
        y_diff_reduced = np.abs(y_predict_reduced - y_test_reduced)
        y_diff_mean_reduced = np.mean(y_diff_reduced)
        print('  Reduced set:')
        print('    Average Deviance: %e ' % y_diff_mean_reduced)
        print('    Maximal Deviance: %e ' % (np.max(y_diff_reduced)))
        forest.plot_features()

        y_diff_plot.append(y_diff_mean)
        ydiff_plot_reduced.append(y_diff_mean_reduced)

    plt.plot(forest_sizes, y_diff_plot, label="All features")
    plt.plot(forest_sizes, ydiff_plot_reduced, label="Reduced Set")
    plt.legend()
    plt.xlabel("Number of Trees")
    plt.ylabel("Average Deviance")
    plt.show()
