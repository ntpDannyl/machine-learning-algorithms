import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor

np.random.seed(42)


class gradient_boosting:
    def __init__(self, trees=10, max_depth=4, alpha=0.2):
        self.trees = trees
        self.max_depth = max_depth
        self.alpha = alpha
        self.learners = []
        self.start_value = None

    def fit(self, X, y):
        self.start_value = np.mean(y)
        y_predict = self.start_value * np.ones(X.shape[0])

        for _ in range(0, self.trees):
            residual = -(y_predict - y)
            learner = DecisionTreeRegressor(max_depth=self.max_depth)
            learner.fit(X, residual)
            self.learners.append(learner)
            y_predict += self.alpha * learner.predict(X)

    def predict(self, X, level=np.inf):
        y = self.start_value * np.ones(X.shape[0])
        for count, learner in enumerate(self.learners):
            if count > level - 1:
                break
            y += self.alpha * learner.predict(X)
        return y


if __name__ == '__main__':
    X = np.linspace(0, 1, 1000)
    yT = np.round(3 * np.cos(2 * np.pi * X) * np.sin(np.pi * X + 0.2))
    y = yT + 0.2 * (np.random.rand(1000) - 0.5)
    X = X.reshape(1000, 1)
    gb = gradient_boosting(trees=20, alpha=0.5, max_depth=2)
    gb.fit(X, y)
    errorList = []
    for i in range(20):
        yP = gb.predict(X, level=i)
        plt.figure()
        plt.scatter(X, y, s=0.5)
        plt.plot(X, yP, c='k', lw=2)
        plt.ylim([-3.1, 1.1])
        plt.title(f'Iteration {i}')
        plt.savefig(f'./gradient_boosting_plots/iteration{i}.png', dpi=300)
        plt.show()
        errorList.append(np.mean(np.abs(yP - yT)))

    plt.figure()
    plt.plot(errorList, lw=2)
    plt.xlabel('Number of Trees')
    plt.ylabel('Mean Squared Error')
    plt.savefig(f'./gradient_boosting_plots/mse.png', dpi=300)
    plt.show()
