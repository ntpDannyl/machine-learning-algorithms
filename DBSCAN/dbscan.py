import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import KDTree

min_points = 5
epsilon = 0.07


class DBSCAN:
    def __init__(self, x, p=2, leaf_size=30):
        self.p = p
        self.leaf_size = leaf_size
        self.x = x
        self.kd_tree = KDTree(self.x, leafsize=self.leaf_size)
        self.epsilon = 0
        self.min_points = 0
        self.visited = None
        self.clusters = None
        self.markers = ['.', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', 'P', 'h', 'H', '+', 'x',
                        'X', 'd', 'D', '|', '_']
        self.colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan',
                       'lightcoral', 'firebrick', 'chocolate', 'saddlebrown', 'sandybrown', 'burlywood', 'gold',
                       'yellowgreen', 'yellow', 'lime', 'aqua', 'dodgerblue', 'darkmagenta', 'slateblue']

    def fit_predict(self, epsilon, min_points):
        self.epsilon = epsilon
        self.min_points = min_points
        c = 0
        self.visited = np.zeros(self.x.shape[0], dtype=bool)
        self.clusters = -10 * np.ones(self.x.shape[0], dtype=int)
        for i in range(self.visited.shape[0]):
            if not self.visited[i]:
                self.visited[i] = True
                p = self.x[i, :]
                n = np.array(self.kd_tree.query_ball_point(p, self.epsilon, p=self.p))
                if n.shape[0] < self.min_points:
                    self.clusters[i] = -1
                else:
                    self.visited[i] = True
                    self._expand_cluster(n, c)
                    c += 1

        print(f'Got {self.clusters.max()+1} clusters and {np.flatnonzero(self.clusters == -1).shape[0] / self.clusters.shape[0]:.2f}% noise.')
        return self.clusters

    def _expand_cluster(self, n, c):
        elements = n.shape[0]
        j = 0
        while j < elements:
            i = n[j]
            if not self.visited[i]:
                self.visited[i] = True
                n_expand = np.array(self.kd_tree.query_ball_point(self.x[i, :], self.epsilon, p=self.p))
                if n_expand.shape[0] >= self.min_points:
                    n = np.hstack((n, n_expand))
                    elements = n.shape[0]
            if self.clusters[i] < 0:
                self.clusters[i] = c
            j = j + 1

    def analyse_epsilon(self, min_points=5):
        distances, _ = self.kd_tree.query(self.x, k=min_points, p=self.p)
        distances = np.max(distances, axis=1)
        fig = plt.figure(1)
        ax = fig.add_subplot(1, 1, 1)
        ax.hist(distances, 10, facecolor='k', alpha=0.5)
        ax.set_xlabel('Distances')
        plt.show()
        return distances

    def plot(self, title='', feature_x=0, feature_y=1, xlim=None, ylim=None):
        try:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            index = np.flatnonzero(self.clusters == -1)
            ax.scatter(self.x[index, feature_x], self.x[index, feature_y], c='black', s=30, marker='*')

            for i in range(0, self.clusters.max() + 1):
                index = np.flatnonzero(self.clusters == i)
                ax.scatter(self.x[index, feature_x], self.x[index, feature_y], c=self.colors[i], s=60, alpha=0.9,
                           marker=self.markers[i])

            if xlim is not None:
                ax.set_xlim(xlim[0], xlim[1])
            if ylim is not None:
                ax.set_ylim(ylim[0], ylim[1])
            ax.set_title(title)
            plt.show()
        except IndexError:
            print('Too many clusters to print. Consider re-analyzing epsilon.')
