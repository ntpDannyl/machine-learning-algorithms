import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor


def boruta(X, y, n):
    # initialize hits counter
    hits = np.zeros((len(X.columns)))

    # repeat 20 times
    for i in range(n):
        # make X_shadow by randomly permuting each column of X
        np.random.seed(i)
        X_shadow = X.apply(np.random.permutation)
        X_boruta = pd.concat([X, X_shadow], axis=1)
        # fit a random forest (suggested max_depth between 3 and 7)
        forest = RandomForestRegressor(max_depth=5, random_state=42)
        forest.fit(X_boruta, y)
        # store feature importance
        feat_imp_X = forest.feature_importances_[:len(X.columns)]
        feat_imp_shadow = forest.feature_importances_[len(X.columns):]
        # compute hits for this trial and add to counter
        hits += (feat_imp_X > feat_imp_shadow.max())

    return hits


if __name__ == '__main__':

    # make X and y
    X = pd.DataFrame({'Age': [25, 32, 47, 51, 62], 'Height': [182, 176, 174, 168, 181], 'Weight': [75, 71, 78, 72, 86]})
    y = pd.Series([20, 32, 45, 55, 61], name='income')
    n = 20

    hits = boruta(X, y, n)

    # draw the probability plot. we don't use a for-loop to position our text better
    x = np.linspace(0, n + 1, (n + 1))
    pmf = [sp.stats.binom.pmf(x, n, .5) for x in range(n + 1)]
    plt.plot(x, pmf, zorder=1)

    plt.scatter(hits[0], pmf[int(hits[0])], color='green', zorder=2)
    plt.text(hits[0], pmf[int(hits[0])] + 0.005, X.columns[0], zorder=2)

    plt.scatter(hits[1], pmf[int(hits[1])], color='blue', zorder=2)
    plt.text(hits[1] - 1.7, pmf[int(hits[1])] + 0.005, X.columns[1], zorder=2)

    plt.scatter(hits[2], pmf[int(hits[2])], color='red', zorder=2)
    plt.text(hits[2], pmf[int(hits[2])] + 0.005, X.columns[2], zorder=2)

    plt.fill_between(x[0:5], pmf[0:5], color='red', alpha=0.3)
    plt.fill_between(x[4:17], pmf[4:17], color='blue', alpha=0.3)
    plt.fill_between(x[16:21], pmf[16:21], color='green', alpha=0.3)
    plt.ylabel('pmf binomial distribution with n=20 and p=0.5')
    plt.xlabel('number of hits in 20 trials')
    plt.savefig('boruta.png', dpi=300)
    plt.show()
