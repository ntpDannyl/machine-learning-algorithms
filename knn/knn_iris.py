import numpy as np
from knn import Knn
from basics.refactor import train_test_split

# PARAMS
np.random.seed(42)
train_percentage = 0.8


if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = dataset[:, 0:4]
    y = dataset[:, 4]

    x_train, x_test, y_train, y_test = train_test_split(x, y, train_percentage)
    errors = 0
    for i in range(len(y_test)):
        predicted = Knn.knn_classification(x_train, y_train, x_test[i, :], 3, None)
        if predicted != y_test[i]:
            errors += 1
            print('%s wurde als %d statt %d klassifiziert' % (str(x_test[i, :]), predicted, y_test[i]))