import numpy as np
import matplotlib.pyplot as plt
from knn import Knn
from matplotlib import cm
from basics.refactor import train_test_split
np.random.seed(42)


if __name__ == '__main__':
    samples = 5000
    p_noise = 0.1
    k = 3
    smear = 0.5
    percent_training = 0.8

    x = np.random.rand(samples, 2)
    y = np.tanh(500 * ((1 / 16) - (x[:, 0] - 0.5) ** 2 - (x[:, 1] - 0.5) ** 2))
    noise = np.random.normal(size=len(y))
    y = (1 + noise * p_noise) * y

    x_train, x_test, y_train, y_test = train_test_split(x, y, percent_training)

    my_regression = Knn()
    my_regression.regression_fit(x_train, y_train)
    y_predicted = my_regression.regression_predict(x_test, k, smear)
    diff = y_predicted - y_test
    print(np.mean(np.abs(diff)))  # mean absolute error

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(x_test[:, 0], x_test[:, 1], y_predicted, alpha=0.6, c=y_predicted, cmap=cm.copper)
    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_zlabel('$y_P$')
    plt.show()
