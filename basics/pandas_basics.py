import pandas as pd
import matplotlib.pyplot as plt


# print just 10 columns of the nations.csv

def series():
    # Series is a bit like a 1D Array but more
    s = pd.Series([3, 5, 6])

    # Prints the labels as well as the values:
    # Labels  Values
    # 0       3
    # 1       5
    # 2       6
    # Labels are also called index, BUT they are NOT the position like in numpy. They could also be a chosen string.
    print(s)

    # iloc = Integer Location. Uses the real location, np.array[2] ≈ s.iloc[2].
    # So we are talking about the real postion, not a label.
    print(s.iloc[0:2])
    print(s.iloc[[0, 2]])

    # Dropping uses a label, not a position. We can't use the next line twice, because the label '1' is dropped.
    s.drop(1, inplace=True)
    print(s)

    # This just prints '3 6', funnily enough not the indices.
    print(s.iloc[0], s.iloc[1])

    # Print the indices (Labels) that are left: [0, 2]
    print(s.index)

    # loc = Location. Change the Value with the label 2 to 7. ! Label = Index ≠ Position !
    # We couldn't use s.loc['2'] = 7 here, because 2 ≠ '2'. It would create a new entry instead.
    s.loc[2] = 7
    print(s)

    # Some more of that ! Label = Index ≠ Position !
    s = pd.Series([3, 5], index=['a', 'b'])
    print(s)
    print(s.loc['b'])

    # Now this line creates a new entry, because there is no label 2.
    s.loc[2] = 7
    print(s)
    print(s.index)  # dtype='object'

    # This still creates a new entry. Type-Juggling ftw.
    s.loc['2'] = 42
    print(s)


def dataframes():
    df = pd.DataFrame([[1, 2, 3], [4, 5, 6]], index=['table', 'chair'], columns=['w', 'h', 'd'])
    #           w   h   d ← Columns
    # table     1   2   3
    # chair     4   5   6
    # ↑ Labels
    print(df)
    print(df.index)  # dtype='object'
    print(df.columns)  # dtype='object'

    df = pd.read_csv('./../problems/nations.csv')
    print(df.head())
    # only using [0:2] would give us the first two rows
    print(df.iloc[0:2, 0:5])

    # loc can be used with labels once again, ...
    # BUT loc[0:2] returns the first THREE columns where iloc[0:2] returns only TWO.
    print(df.loc[0:2, ['income', 'neonat_mortal_rate']])

    # Returns a Pandas-Series with Length of 4975 and dtype: bool
    bool_indexing_for_south_asia = df.loc[:, 'region'] == 'South Asia'
    print(bool_indexing_for_south_asia)

    # count entries from the region South Asia: 200
    print(bool_indexing_for_south_asia.sum())

    # We can use the bool indexing with loc as well, not with iloc though.
    # iloc needs an np.ndarray. We could convert it with: bool_indexing_for_south_asia.to_numpy()
    max_birth = df.loc[bool_indexing_for_south_asia, 'birth_rate'].max()
    min_birth = df.loc[bool_indexing_for_south_asia, df.columns[7]].min()
    print(f'The max birthrate in the South Asia region is {max_birth:.2f} while the min is {min_birth:.2f}.')

    # df[] seems to be shorthand for df.loc[]
    print(df[df.iso3c == 'AFG'])

    # df.drop(columns='iso3c', inplace=True) ≈ df = df.drop(columns='iso3c')
    # df.drop(columns='iso3c', inplace=True)
    # print(df)

    # With this we could use iso3c as index instead of a feature.
    # The problem is, that rows are not uniquely adressable via the label anymore.
    df = df.set_index('iso3c')
    print(df.loc['AFG'])  # prints all rows with the label AFG now.

    # Merging of dataframes:
    df1 = pd.DataFrame([['Alice', '314159'], ['Bob', '271828'], ['Ingo', '662607']], columns=['Name', 'Phone'])
    df2 = pd.DataFrame([['Alice', 'Cat'], ['Bob', 'Dog'], ['Carola', 'Horse']], columns=['Name', 'Pet'])
    print(pd.merge(df1, df2, on='Name'))  # only returns 2 rows Bob and Alice which are in both DataFrames.

    # How to rename columns, if you want that
    # df.rename(columns={'country': 'Staat', 'year': 'Jahr', ...}, inplace=True)

    print(df.country.unique())  # Get all unique countries

    # How to plot all numeric features
    df.hist(bins=25)

    # Because pd uses plt we can combine the functions quite easily
    plt.figure()
    ax = df.loc[:, 'income'].hist()
    ax.set_xlabel('Income')
    ax.set_ylabel('Frequency')
    df.plot.scatter(x='year', y='neonat_mortal_rate', c='gdp_percap', cmap=plt.cm.tab20, alpha=0.4)

    # Only plot one country
    df[df.country == 'Iraq'].plot.scatter(x='year', y='neonat_mortal_rate', c='gdp_percap', cmap=plt.cm.tab20, alpha=0.4)
    plt.show()

    # Get useful information about your numerical data. Use include='all' to add categorical data
    print(df.describe())
    print(df.isna().sum())  # notna() also available


if __name__ == '__main__':
    dataframes()
