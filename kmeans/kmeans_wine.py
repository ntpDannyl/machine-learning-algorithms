import numpy as np
from kmeans import KMeans
from basics.refactor import normalize_x

np.random.seed(42)
n_clusters = 3
max_iterations = 42

if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/wine.csv", delimiter=",")

    x = dataset[:, 1:14]
    y = dataset[:, 0] - 1

    x = normalize_x(x)

    for p in (1, 2, 4):
        kmeans = KMeans(n_clusters, p)
        _, cluster = kmeans.fit(x, max_iterations)

        index1, count1 = np.unique(cluster[0:59], return_counts=True)
        index2, count2 = np.unique(cluster[59:130], return_counts=True)
        index3, count3 = np.unique(cluster[130:178], return_counts=True)

        max1 = index1[np.unravel_index(count1.argmax(), count1.shape)]
        max2 = index2[np.unravel_index(count2.argmax(), count2.shape)]
        max3 = index3[np.unravel_index(count3.argmax(), count3.shape)]

        y[0:59] = max1
        y[59:130] = max2
        y[130:178] = max3

        counter = 0
        for i in range(0, len(y)):
            if int(cluster[i, None]) == int(y[i, None]):
                counter = counter + 1

        print(f'{counter}/{len(y)} correctly clustered with {n_clusters} clusters and {p}-norm: {counter / len(y) * 100:.2f}%')
