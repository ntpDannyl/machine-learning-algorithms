import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from trees.CART import CART
from basics.refactor import train_test_split, standardize_x
from basics.analyze import plot_correlation_matrix, sequential_forward_selection, sequential_backward_selection

np.random.seed(42)
plt.rcParams.update({'figure.max_open_warning': 0})

if __name__ == '__main__':
    # 0: Create a useful dataset from nations.csv

    df = pd.read_csv('./../problems/nations.csv')
    df.drop(columns='iso3c', inplace=True)

    # We need to replace the string with a numerical value. We use the average of the income-groups for that
    df['income'].replace(to_replace=['Low income'], value=647.5, inplace=True)
    df['income'].replace(to_replace=['Lower middle income'], value=2445.5, inplace=True)
    df['income'].replace(to_replace=['Upper middle income'], value=7975.5, inplace=True)
    df['income'].replace(to_replace=['High income'], value=27218.0, inplace=True)
    df['income'].replace(to_replace=['High income: OECD'], value=42380.0, inplace=True)

    df['region'] = pd.factorize(df.region)[0]  # every region gets it's own number

    # we just keep the rows where population, life expectancy and birthrate are complete
    df = df[df.population.notna() & df.life_expect.notna() & df.birth_rate.notna()]

    # get the ID's of all NaNs in the feature gdp per capita and the neonate mortal rate

    df = df[df.gdp_percap.notna() | df.neonat_mortal_rate.notna()]  # if both entries are NaN we don't keep them

    df_numbers_only = df.select_dtypes('number')  # drop all features that aren't numbers. In our case it's "country".
    y = df_numbers_only.life_expect
    x = standardize_x(df_numbers_only.drop(columns=['life_expect']))
    ids_without_nan = df.gdp_percap.notna() & df.neonat_mortal_rate.notna()
    x_train, x_test, y_train, y_test = train_test_split(x[ids_without_nan], y[ids_without_nan], 0.7, using_pd=True)

    # 1: Plot the trend line between different features, which can be an indicator for correlation
    # Needs at least ordinal numbers to work and can only show linear correlation
    dfSort = df[df.notna().all(axis=1)]
    dfSort = dfSort[dfSort.year == 2011]
    dfSort = dfSort.select_dtypes('number').drop(columns=['region', 'income', 'year'])
    dfSort = standardize_x(dfSort)
    for feature1 in dfSort.columns:
        for feature2 in dfSort.columns:
            if feature1 == feature2:
                continue  # correlation with itself is always 1
            plt.figure()
            plt.scatter(dfSort.loc[:, feature2], dfSort.loc[:, feature1], c='k')
            z = np.polyfit(dfSort.loc[:, feature2], dfSort.loc[:, feature1], 1)
            plt.plot(dfSort.loc[:, feature2], z[0] * dfSort.loc[:, feature2] + z[1], 'r--', lw=3)
            plt.xlabel(feature2 + ' [standardized]', fontsize=14)
            plt.ylabel(feature1 + ' [standardized]', fontsize=14)
            # will not show data-points for countries with big populations but the overall graph will be better
            plt.xlim([-2.5, 2.5])
            plt.ylim([-2.5, 2.5])
    plot_correlation_matrix(df)

    # 2: We do something that is normally not possible in bigger datasets:
    # We try out a CART-Prediction with ever feature-combination to see the MAE in combination with the correlation
    full_tree = CART(is_regression=True)
    full_tree.fit(x_train.to_numpy(), y_train.to_numpy())
    yP = full_tree.predict(x_test.to_numpy())
    print(f'CART for all features: {np.mean(np.abs(yP - y_test.to_numpy())):.3f}')
    for feature1 in x_train.columns:
        for feature2 in x_train.columns:
            if feature1 == feature2:
                continue
            fTrain = x_train.loc[:, [feature1, feature2]]
            fTest = x_test.loc[:, [feature1, feature2]]
            reduTree = CART(is_regression=True)
            reduTree.fit(fTrain.to_numpy(), y_train.to_numpy())
            yP = reduTree.predict(fTest.to_numpy())
            print(f'CART ({feature1}, {feature2}): {np.mean(np.abs(yP - y_test.to_numpy())):.3f}')
    # We see, that birth_rate & neonat_mortal_rate has a good correlation but an MAE of 3.054.
    # There are better CARTs with population or region, so linear correlation is just another indicator, not the solution

    # 3: We print the life expectancy over the years, just to see that not everything was better in the past
    plt.figure()
    plt.plot(df[df.country == 'Germany'].year, df[df.country == 'Germany'].life_expect, 'k-', label='Germany')
    plt.plot(df[df.country == 'Samoa'].year, df[df.country == 'Samoa'].life_expect, 'k:', label='Samoa')
    plt.plot(df[df.country == 'Malta'].year, df[df.country == 'Malta'].life_expect, 'r--', label='Malta')
    plt.legend()
    plt.xlabel('years')
    plt.ylabel('life_expect')

    # 4: get selected features via the greedy algorithm
    print(sequential_backward_selection(x_train.to_numpy(), y_train.to_numpy(), k=2))
    print(sequential_forward_selection(x_train.to_numpy(), y_train.to_numpy(), k=2))
