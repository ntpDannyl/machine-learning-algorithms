import numpy as np
import matplotlib.pyplot as plt
from kmeans import KMeans
from basics.refactor import normalize_x

np.random.seed(42)
max_iterations = 42
norm_order = 2
normalize = True
n_clusters_4d = 3

if __name__ == '__main__':

    iris_dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = iris_dataset[:, 0:4]
    y = iris_dataset[:, 4] - 1  # we want to start counting at 0
    if normalize:
        x = normalize_x(x)

    sets_and_clusters = [
        {
            'dataset': x[:, [0, 1]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [2, 3]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [0, 2]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [1, 3]],
            'n_clusters': 3
        }
    ]

    for entry in sets_and_clusters:  # all possible 2D-Combinations
        dataset = entry['dataset']
        n_clusters = entry['n_clusters']
        kmeans = KMeans(n_clusters, norm_order)
        _, cluster = kmeans.fit(dataset, max_iterations)

        fig = plt.figure(1)
        ax = fig.add_subplot(1, 1, 1)
        markers = ['*', 's', '<', 'o', 'X', '8', 'p', 'h', 'H', 'D', 'd', 'P']
        colorName = ['black', 'red', 'blue', 'green']
        for j in range(n_clusters):
            index = np.flatnonzero(cluster == j)
            ax.scatter(dataset[index, 0], dataset[index, 1], c=colorName[j], s=60, alpha=0.2, marker=markers[j])
        for i in range(len(kmeans.fitHistory)):
            for j in range(n_clusters):
                ax.text(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1], str(i), style='italic',
                        bbox={'facecolor': 'white', 'alpha': 0.7, 'pad': 2}, color=colorName[j])
                ax.annotate(str(i), xy=(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1]), color=colorName[j])
                # ax.scatter(kmeans.fitHistory[i][j, 0], kmeans.fitHistory[i][j, 1], c='m', s=60, marker=markers[j])

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.grid(True, linestyle='-', color='0.75')
        ax.set_aspect('equal', 'datalim')
        plt.show()

    # 4D
    kmeans = KMeans(n_clusters_4d, norm_order)
    _, cluster = kmeans.fit(x, max_iterations)

    index1, count1 = np.unique(cluster[0:50], return_counts=True)
    index2, count2 = np.unique(cluster[50:100], return_counts=True)
    index3, count3 = np.unique(cluster[100:150], return_counts=True)

    max1 = index1[np.unravel_index(count1.argmax(), count1.shape)]
    max2 = index2[np.unravel_index(count2.argmax(), count2.shape)]
    max3 = index3[np.unravel_index(count3.argmax(), count3.shape)]

    y[0:50] = max1
    y[50:100] = max2
    y[100:150] = max3

    counter = 0
    for i in range(0, len(y)):
        if int(cluster[i, None]) == int(y[i, None]):
            counter = counter + 1

    print(f'{counter}/{len(y)} correctly clustered with {n_clusters_4d} clusters and {norm_order}-norm: {counter / len(y) * 100:.2f}%')