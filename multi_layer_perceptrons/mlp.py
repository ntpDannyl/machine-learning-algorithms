import copy
import numpy as np
from activationfunctions import sigmoid
# reproducibility
np.random.seed(42)


class Mlp:
    def __init__(self, hiddenlayer, normalize):
        self.error = []
        self.hidden_layer = hiddenlayer
        self.normalize = normalize
        self.x_min = 0.0
        self.x_max = 1.0
        self.weights = []
        self.input_layer = 0
        self.output_layer = 1

    def _init_weights(self):  # initialize random weights so that the error won't be distributed equally
        self.weights.append((np.random.rand(self.hidden_layer[0], self.input_layer) - 0.5))
        self.weights.append((np.random.rand(self.hidden_layer[1], self.hidden_layer[0]) - 0.5))
        self.weights.append((np.random.rand(self.output_layer, self.hidden_layer[1]) - 0.5))

    def _calc_layers(self, x):  # calculate the layers as in section 2.2
        o1 = sigmoid(self.weights[0] @ x.T)
        o2 = sigmoid(self.weights[1] @ o1)
        y = (self.weights[len(self.weights) - 1] @ o2).T
        return y

    def predict(self, x):  # norm input, add bias, calculate output
        x = (x - self.x_min) / (self.x_max - self.x_min)
        x = np.hstack((x, np.ones(x.shape[0])[:, None]))
        y = self._calc_layers(x)
        return y

    def fit(self, x, y, eta, max_iterations, epsilon):
        # norm batch if normalize is True.
        # We don't use our basics.refactor function here, because we need the global max and min for a correct prediction
        self.x_min = x.min(axis=0) if self.normalize else 0
        self.x_max = x.max(axis=0) if self.normalize else 1
        x = (x - self.x_min) / (self.x_max - self.x_min)
        # add bias neuron
        x = np.hstack((x, np.ones(x.shape[0])[:, None]))
        if len(y.shape) == 1:
            y = y[:, None]
        self.input_layer = x.shape[1]
        self._init_weights()

        self.train(x, y, eta, max_iterations, epsilon)

    def train(self, X, Y, eta, max_iterations, epsilon):
        delta_weights = []
        for i in range(len(self.weights)):
            delta_weights.append(np.zeros_like(self.weights[i]))
        yp = self._calc_layers(X)

        mean_error = np.sum((Y - yp) ** 2) / X.shape[0]
        min_error = mean_error
        min_weights = copy.deepcopy(self.weights)
        mix_set = np.random.choice(X.shape[0], X.shape[0], replace=False)
        counter = 0

        # learning algorithm
        while mean_error > epsilon and counter < max_iterations:
            counter += 1
            for i in mix_set:
                x = X[i, :]
                o1 = sigmoid(self.weights[0] @ x.T)
                o2 = sigmoid(self.weights[1] @ o1)
                temp = self.weights[2] * o2 * (1 - o2)[None, :]
                # calculate the weights as in section 2.2
                delta_weights[2] = o2
                delta_weights[1] = temp.T @ o1[:, None].T
                delta_weights[0] = (o1 * (1 - o1) * (temp @ self.weights[1])).T @ x[:, None].T

                yp = self._calc_layers(x)
                yfactor = np.sum(Y[i] - yp)
                for j in range(len(self.weights)):
                    self.weights[j] += eta * yfactor * delta_weights[j]

            yp = self._calc_layers(X)

            mean_error = (np.sum((Y - yp) ** 2) / X.shape[0])
            self.error.append(mean_error)
            if mean_error < min_error:
                min_error = mean_error
                min_weights = copy.deepcopy(self.weights)
        self.weights = copy.deepcopy(min_weights)
