import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense
from basics.refactor import standardize_x
from pca import PCA
from trees.random_forest import RandomForest

np.random.seed(42)

if __name__ == '__main__':
    y_diffs_mean = []
    y_diffs_max = []

    dataset = np.loadtxt('.././problems/bike_sharing_dataset.csv', delimiter=",", skiprows=1)
    dataset = np.delete(dataset, np.flatnonzero(dataset[:, 8] == 4), axis=0)
    features = dataset[:, 0:13]

    X = np.delete(features, 3, axis=1)
    X = standardize_x(X)
    y = dataset[:, 15]

    pca = PCA(X)

    pca.plot()

    for i in range(5, 13):
        x_projected, _ = pca.get_projection(i)

        days_train = np.flatnonzero(features[:, 3] <= 20)
        days_test = np.flatnonzero(features[:, 3] > 20)

        x_train = x_projected[days_train, :]
        y_train = y[days_train]

        x_test = x_projected[days_test, :]
        y_test = y[days_test]

        earlystop = keras.callbacks.EarlyStopping(monitor="loss", patience=20, verbose=False)
        checkpoint = keras.callbacks.ModelCheckpoint("bestW.h5", monitor="loss", verbose=False, save_weights_only=True, save_best_only=True)

        """net = Sequential()
        net.add(Dense(90, input_dim=i, kernel_initializer="normal", activation="relu"))
        net.add(Dense(90, kernel_initializer="random_uniform", activation="relu"))
        net.add(Dense(1, kernel_initializer="normal", activation="linear"))
        net.compile(loss="mean_squared_error", optimizer="adam", metrics=["accuracy"])
        history = net.fit(x_train, y_train, epochs=100, callbacks=[earlystop, checkpoint], verbose=False)

        y_predict = net.predict(x_test)
        y_diff = np.abs(y_predict - y_test[:, None])  # we have to transform y_test from shape (5917,) to (5917, 1)"""

        # If you want to use a forest instead a neural net
        forest = RandomForest(trees=10, threshold=10 ** -2, x_decimals=3, is_regression=True)
        forest.fit(x_train, y_train)
        y_predict = np.round(forest.predict(x_test))
        y_diff = np.abs(y_predict - y_test)

        y_diffs_mean.append(np.mean(y_diff))
        y_diffs_max.append(np.max(y_diff))
        print(f'Average Deviance with {i} features: {y_diffs_mean[-1]}')
        print(f'Maximal Deviance with {i} features: {y_diffs_max[-1]}')

    x = range(5, 13)
    plt.plot(x, y_diffs_mean, label="y_diff_mean")
    plt.plot(x, y_diffs_max, label="y_diff_max")
    plt.xlabel('Number of Features')
    plt.ylabel('Deviance')
    plt.legend()
    plt.show()

    """
    We see that Month and Hour are not as important and can be deleted, as seen in pca.eigenvalues
    The neural net is better here than a random forest and utilizes the features better, but one can still see a trend.
    Also if you compare the stats of the random_forest_bike_sharing.py with this script you see, that the first one is 
    more precise in it's predictions with the only difference being the PCA.
    """
