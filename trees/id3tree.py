import numpy as np
from binary_tree import BinaryTree
from basics.analyze import calc_conditional_entropy


class ID3BinaryTree:
    def __init__(self):
        self.bTree = None

    def _choose_feature(self, X, y):
        # calculate the conditional entropy
        h = np.zeros(X.shape[1])
        for i in range(len(h)):
            h[i] = calc_conditional_entropy(y, X, i)
        return np.argmin(h)  # and choose the lowest one

    def _generate_tree(self, x, y, parent_node, branch, a):
        if parent_node is None:  # we have to create a root-node first
            a = np.arange(x.shape[1])
        else:
            if len(y) == np.sum(y):  # only positive conditions left
                self.bTree.add_node(parent_node, branch, True)
                return
            elif 0 == np.sum(y):
                self.bTree.add_node(parent_node, branch, False)
                return
            common_value = True if np.sum(y) > len(y) / 2 else False
            if x.shape[0] == 0:  # no features left
                self.bTree.add_node(parent_node, branch, common_value)
                return
        choose_a = self._choose_feature(x, y)

        if parent_node is None:  # we have to create a root-node first
            self.bTree = BinaryTree(choose_a, True, '=')
            my_no = 0
        else:  # create a new node
            my_no = self.bTree.add_node(parent_node, branch, True, operator='=', var_no=a[choose_a])

        # delete feature x in a
        index = np.flatnonzero(np.logical_and(x[:, choose_a], 1))
        x = np.delete(x, choose_a, axis=1)
        a = np.delete(a, choose_a, axis=0)

        # split x
        x_true = x[index, :]
        y_true = y[index]
        x_false = np.delete(x, index, axis=0)
        y_false = np.delete(y, index, axis=0)
        if x_true.shape[0] > 0:
            self._generate_tree(x_true, y_true, my_no, True, a)
        else:
            self.bTree.add_node(my_no, True, common_value)
        if x_false.shape[0] > 0:
            self._generate_tree(x_false, y_false, my_no, False, a)
        else:
            self.bTree.add_node(my_no, False, common_value)
        return

    def fit(self, x, y):
        self._generate_tree(x, y, None, None, None)

    def predict(self, x):
        return self.bTree.eval(x)

    def decision_path(self, x):
        return self.bTree.trace(x)

    def weighted_path_length(self, x):
        return self.bTree.weightedPathLength(x)

    def number_of_leafs(self):
        return self.bTree.numberOfLeafs()


if __name__ == '__main__':
    bicycle_dataset = np.loadtxt("./../problems/bicycle_decision_simple.csv", delimiter=",", skiprows=1)
    x = bicycle_dataset[:, 0:4]
    y = bicycle_dataset[:, 4]
    my_tree = ID3BinaryTree()
    my_tree.fit(x, y)
    print(my_tree.bTree.print())
    print(my_tree.predict(np.array([True, False, False, False]).reshape(1, 4)))
