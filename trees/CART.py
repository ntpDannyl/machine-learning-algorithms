import numpy as np
from trees.binary_tree import BinaryTree
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# this implementation has O(n_f * n_t² * log(n_t)) with n_f = number of features and n_t = lines of data
# it is possible to reduce the CART to O(n_f * n_t * log(n_t)), with a few tricks.
# See https://scikit-learn.org/stable/modules/tree.html for more.


class CART:
    def __init__(self, threshold=0.1, x_decimals=8, min_leaf_node_size=3, is_regression=False, is_random_forest=False, n=0):
        self.tree = None
        self.threshold = threshold
        self.x_decimals = x_decimals
        self.min_leaf_node_size = min_leaf_node_size
        self.n = n
        self.is_regression = is_regression
        self.is_random_forest = is_random_forest

    def _calculate_lrss(self, y):
        return np.sum((y - np.sum(y) / len(y)) ** 2) # L2 with y_mean = np.sum(y) / len(y)

    def _calculate_gini_impurity(self, y):
        _, counts = np.unique(y, return_counts=True)
        return 1 - np.sum((counts / len(y)) ** 2)  # Gini-Impurity with N = counts / len(y)

    def _best_split(self, X, y, feature):
        # classification uses Gini Impurity while regression ueses RSS
        decision_factor = np.inf if self.is_regression else 1
        best_split = np.inf
        x_sort = np.unique(X[:, feature].round(self.x_decimals))
        x_diff = (x_sort[1:len(x_sort)] + x_sort[0:len(x_sort) - 1]) / 2
        for i in range(x_diff.shape[0]):
            index = np.less(X[:, feature], x_diff[i])
            df1 = self._calculate_lrss(y[index]) if self.is_regression else self._calculate_gini_impurity(y[index])
            df2 = self._calculate_lrss(y[~index]) if self.is_regression else self._calculate_gini_impurity(y[~index])
            df_split = df1 + df2 if self.is_regression else len(y[index]) / len(y) * df1 + len(y[~index]) / len(y) * df2
            if decision_factor > df_split:
                decision_factor = df_split
                best_split = x_diff[i]
        return best_split, decision_factor

    def _choose_feature(self, x, y):
        best_split = np.zeros(x.shape[1])
        if self.is_random_forest:
            g = np.inf * np.ones(x.shape[1])
            if self.n == 0:
                feature = np.arange(x.shape[1])
            elif self.n == -1:
                feature = np.random.choice(x.shape[1], int(np.sqrt(x.shape[1])), replace=False)
            else:
                feature = np.random.choice(x.shape[1], self.n, replace=False)
            for i in feature:
                (best_split[i], g[i]) = self._best_split(x, y, i)
        else:
            g = np.zeros(x.shape[1])
            for i in range(x.shape[1]):
                (best_split[i], g[i]) = self._best_split(x, y, i)
        smallest = np.argmin(g)
        return g[smallest], best_split[smallest], smallest


    def _compute_value(self, y):
        if self.is_regression:
            return np.sum(y) / len(y)
        unique, counts = np.unique(y, return_counts=True)
        i = np.argmax(counts)
        return unique[i]

    def _gen_tree(self, x, y, parent_node, branch):
        common_value = self._compute_value(y)
        initial_decision_factor = self._calculate_lrss(y) if self.is_regression else self._calculate_gini_impurity(y)
        if initial_decision_factor < self.threshold or x.shape[0] <= self.min_leaf_node_size:
            self.tree.add_node(parent_node, branch, common_value)
            return

        (decision_factor, bestSplit, chooseA) = self._choose_feature(x, y)
        if decision_factor > 0.98 * initial_decision_factor:
            self.tree.add_node(parent_node, branch, common_value)
            return

        if parent_node is None:
            self.tree = BinaryTree(chooseA, bestSplit, '<')
            node_number = 0
        else:
            node_number = self.tree.add_node(parent_node, branch, bestSplit, operator='<', var_no=chooseA)

        index = np.less(x[:, chooseA], bestSplit)
        x_true = x[index, :]
        y_true = y[index]
        x_false = x[~index, :]
        y_false = y[~index]

        if x_true.shape[0] > self.min_leaf_node_size:
            self._gen_tree(x_true, y_true, node_number, True)
        else:
            common_value = self._compute_value(y_true)
            self.tree.add_node(node_number, True, common_value)
        if x_false.shape[0] > self.min_leaf_node_size:
            self._gen_tree(x_false, y_false, node_number, False)
        else:
            common_value = self._compute_value(y_false)
            self.tree.add_node(node_number, False, common_value)
        return

    def fit(self, x, y):
        self._gen_tree(x, y, None, None)

    def predict(self, x):
        return self.tree.eval(x)

    def decision_path(self, x):
        return self.tree.trace(x)

    def weighted_path_length(self, x):
        return self.tree.weighted_path_length(x)

    def number_of_leafs(self):
        return self.tree.number_of_leafs()


if __name__ == '__main__':  # Test for regression
    np.random.seed(42)
    numberOfSamples = 10000
    x = np.random.rand(numberOfSamples, 2)
    y = (np.sin(2 * np.pi * x[:, 0]) + np.cos(np.pi * x[:, 1])) * np.exp(1 - x[:, 0] ** 2 - x[:, 1] ** 2)

    main_set = np.arange(0, x.shape[0])
    training_set = np.random.choice(x.shape[0], int(0.8 * x.shape[0]), replace=False)
    test_set = np.delete(main_set, training_set)

    regressionError = np.zeros(5)
    for i in range(5):
        errorFactor = 1 + 2 * (np.random.rand(training_set.shape[0]) - 0.5) * 0.05 * i  # 0.05 * i = Error rate
        x_train = x[training_set, :]
        y_train = y[training_set] * errorFactor
        x_test = x[test_set, :]
        y_test = y[test_set]

        cart = CART(x_decimals=3, is_regression=True)
        cart.fit(x_train, y_train)
        y_predict = cart.predict(x_test)
        y_diff = np.abs(y_predict - y_test)
        regressionError[i] = np.mean(y_diff)

        fig1 = plt.figure(1)
        ax = fig1.add_subplot(1, 1, 1, projection='3d')
        ax.scatter(x_test[:, 0], x_test[:, 1], y_predict, alpha=0.6, c=y_predict, cmap='gray')
        ax.set_xlabel('x[0]')
        ax.set_ylabel('x[1]')
        ax.set_zlabel('yPredict')
        plt.show()

    fig2 = plt.figure(2)
    ax = fig2.add_subplot(1, 1, 1)
    x = np.arange(0, 0.25, 0.05)
    ax.plot(x, regressionError, 'o-', c='k')
    ax.set_xlabel('% Noise')
    ax.set_ylabel('Mean Absolute Error')


