import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from scipy.spatial.distance import pdist
from problems.clustering_problems import four_balls, two_moons, two_circles, just_noise, mouse

detect_outliers = True

markers = ['.', ',', 'o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p', 'P', 'h', 'H', '+', 'x',
           'X', 'd', 'D', '|', '_']
colors = ['blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan',
          'lightcoral', 'firebrick', 'chocolate', 'saddlebrown', 'sandybrown', 'burlywood', 'gold',
          'yellowgreen', 'yellow', 'lime', 'aqua', 'dodgerblue', 'darkmagenta', 'slateblue']

if __name__ == '__main__':
    plt.close('all')
    for x in [four_balls(400, 400, 400, 400), four_balls(100, 100, 400, 400), two_moons(240, 0), two_circles(800), mouse(150, 150, 700), just_noise(1000)]:
        for method in ['single', 'complete', 'average', 'centroid']:
            """
            This method builds clusters on its own. 
            The biggest 4 are our main cluster, the rest are handles like outliers.
            It's okay-ish for single-linkage.
            """
            D = pdist(x, metric='euclidean')
            Z = linkage(D, method, metric='euclidean')

            fig = plt.figure(1)
            ax = fig.add_subplot(1, 1, 1)
            if detect_outliers:
                clusters = fcluster(Z, 0.5, criterion='distance')
                _, counts = np.unique(clusters, return_counts=True)
                big4 = np.argsort(counts)[-4:]

                big4_indices = np.flatnonzero(clusters == (big4[0] + 1))
                plt.scatter(x[big4_indices, 0], x[big4_indices, 1], marker=markers[0], c=colors[0])
                for i in range(1, len(big4)):
                    index = np.flatnonzero(clusters == (big4[i] + 1))
                    plt.scatter(x[index, 0], x[index, 1], marker=markers[i], c=colors[i])
                    big4_indices = np.hstack((big4_indices, index))

                outlierIndex = np.arange(x.shape[0])
                outlierIndex = np.delete(outlierIndex, big4_indices)
                plt.scatter(x[outlierIndex, 0], x[outlierIndex, 1], marker='*', c='black')
                ax.set_title(method)
                plt.show()
            else:
                '''
                It would be better to tell the algorithm the amount of clusters for each problem.
                For example the two moons problem should have a 2 instead of a 4 for maxclust.
                However: I wanted to check what happens when we tell each algorithm that there could be a maximum of 
                4 clusters. Because in real life we don't know the number of clusters. It didn't work that well.
                
                If you want you can build a "sets_and_clusters"-array like in kmeans_clustering_problems.py
                '''
                c = fcluster(Z, 4, criterion='maxclust')
                for i in range(c.max()):
                    index = np.flatnonzero(c == i + 1)
                    ax.scatter(x[index, 0], x[index, 1], c=colors[i], s=60, alpha=0.2, marker=markers[i])
                ax.set_title(method)

                plt.show()
            #fig = plt.figure(2)
            #dendrogram(Z, truncate_mode='lastp', p=12, link_color_func=lambda k: 'k')
