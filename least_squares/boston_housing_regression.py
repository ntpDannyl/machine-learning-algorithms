import numpy as np
import matplotlib.pyplot as plt
from basics.refactor import train_test_split
np.random.seed(42)
train_percentage = 0.8

if __name__ == '__main__':
    x = np.loadtxt("./../problems/BostonFeature.csv", delimiter=",")
    y = np.loadtxt("./../problems/BostonTarget.csv", delimiter=",")
    x_train, x_test, y_train, y_test = train_test_split(x, y, train_percentage)

    a = np.ones((x_train.shape[0], 14))
    a[:, 1:14] = x_train
    max_value = np.max(a, axis=0)
    a = a / max_value
    (u, _, a_rank, _) = np.linalg.lstsq(a, y_train)
    r = a @ u - y_train
    print(np.linalg.norm(r) / r.shape[0], np.mean(np.abs(r)), np.max(np.abs(r)))
    print(u)

    b = np.ones((x_test.shape[0], 14))
    b[:, 1:14] = x_test
    b = b / max_value
    y_predict = b @ u
    r_t = y_predict - y_test
    print(np.linalg.norm(r_t) / r_t.shape[0], np.mean(np.abs(r_t)), np.max(np.abs(r)))

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 2, 1)
    ax.set_title('Verteilung der Abweichungen auf der Trainingsmenge')
    ax.hist(r)
    ax.set_xlabel('Abweichung in Tausenden')
    ax.set_ylabel('Anzahl')
    ax = fig.add_subplot(1, 2, 2)
    ax.set_title('Verteilung der Abweichungen auf der Testmenge')
    ax.hist(r_t)
    ax.set_xlabel('Abweichung in Tausenden')
    ax.set_ylabel('Anzahl')
    plt.show()
