import numpy as np
import matplotlib.pyplot as plt
from problems.clustering_problems import four_balls, two_moons, two_circles, just_noise, mouse
from fuzzy_c_means import FuzzyCMeans

np.random.seed(42)

epsilon = 10 ** -3
norm_order = 2
max_iterations = 42


if __name__ == '__main__':
    sets_and_clusters = [
        {
            'dataset': four_balls(400, 400, 400, 400),
            'n_clusters': 4
        },
        {
            'dataset': two_moons(240, 0),
            'n_clusters': 2
        },
        {
            'dataset': two_circles(800),
            'n_clusters': 2
        },
        {
            'dataset': mouse(150, 150, 700),
            'n_clusters': 3
        },
        {
            'dataset': just_noise(1000),
            'n_clusters': 2
        },
    ]

    for entry in sets_and_clusters:
        dataset = entry['dataset']
        n_clusters = entry['n_clusters']
        fuzzy_c_means = FuzzyCMeans(n_clusters, norm_order)
        _, W = fuzzy_c_means.fit(dataset, max_iterations, epsilon)

        cluster = np.argmax(W, axis=1)
        for i in range(n_clusters):
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.scatter(dataset[:, 0], dataset[:, 1], c=W[:, i], s=60, cmap='binary')
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.grid(True, linestyle='-', color='0.75')
            ax.set_aspect('equal', 'datalim')
            plt.show()
