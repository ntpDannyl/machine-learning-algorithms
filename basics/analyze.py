import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from trees.CART import CART
from basics.refactor import standardize_x

#mpl.rcParams['figure.dpi'] = 300
mpl.use('WebAgg')


def analyze_null(dataset):
    if not type(dataset) == pd.core.frame.DataFrame:
        for i in range(dataset.shape[1]):
            print(f'Feature {i} has {np.sum(np.isnan(dataset[:, i]))} nones.')
        return
    print(dataset.isna().sum())


def calc_weighted_self_information(x):
    return 0 if x <= 0 else x * np.log2(x)


def calc_conditional_entropy(y, dataset, feature):
    dataset_size = dataset.shape[0]
    dataset = dataset.astype(bool)
    true_feature_database = np.sum(dataset[:, feature])
    false_feature_database = dataset_size - true_feature_database
    p_feature_true = true_feature_database / dataset_size
    p_feature_false = false_feature_database / dataset_size

    h_true = 0
    h_false = 0
    if p_feature_true > 0:
        p_ab_true = true_feature_database - np.sum(np.logical_and(dataset[:, feature], y))
        p_ab_false = true_feature_database - p_ab_true
        p_ab_true = p_ab_true / true_feature_database
        p_ab_false = p_ab_false / true_feature_database
        h_true = p_feature_true * (calc_weighted_self_information(p_ab_false) + calc_weighted_self_information(p_ab_true))
    if p_feature_false > 0:
        p_ab_true = false_feature_database - np.sum(np.logical_and(~dataset[:, feature], y))
        p_ab_false = false_feature_database - p_ab_true
        p_ab_true = p_ab_true / false_feature_database
        p_ab_false = p_ab_false / false_feature_database
        h_false = p_feature_false * (calc_weighted_self_information(p_ab_false) + calc_weighted_self_information(p_ab_true))

    return -h_true - h_false  # h


def sequential_forward_selection(x, y, k):
    main_set = np.arange(0, x.shape[0])
    validation_set = np.random.choice(x.shape[0], int(x.shape[0] * 0.25), replace=False)
    train_set = np.delete(main_set, validation_set)
    features_left = np.arange(0, x.shape[1])
    suggested_features = np.zeros(1, dtype=int)
    l = 0
    while l < k:
        q = np.inf * np.ones(x.shape[1])
        for i in features_left:
            suggested_features[l] = i
            reduced_tree = CART(is_regression=True)
            reduced_tree.fit(x[np.ix_(train_set, suggested_features)], y[train_set])
            error = y[validation_set] - reduced_tree.predict(x[np.ix_(validation_set, suggested_features)])
            q[i] = np.mean(np.abs(error))
        i = np.argmin(q)
        suggested_features[l] = i
        features_left = np.delete(features_left, np.argwhere(features_left == i))
        suggested_features = np.hstack((suggested_features, np.array([0])))
        l += 1
    suggested_features = suggested_features[0:l]
    return suggested_features


def sequential_backward_selection(x, y, k):
    l = x.shape[1]
    main_set = np.arange(0, x.shape[0])
    validation_set = np.random.choice(x.shape[0], int(x.shape[0] * 0.25), replace=False)
    train_set = np.delete(main_set, validation_set)
    suggested_features = np.arange(0, l)
    while k < l:
        q = np.zeros(l)
        for i in range(l):
            x_reduced = np.delete(x, i, axis=1)
            cart_reduced = CART(is_regression=True)
            cart_reduced.fit(x_reduced[train_set, :], y[train_set])
            error = y[validation_set] - cart_reduced.predict(x_reduced[validation_set, :])
            q[i] = np.mean(np.abs(error))
        i = np.argmin(q)
        suggested_features = np.delete(suggested_features, i)
        x = np.delete(x, i, axis=1)
        l -= 1
    return suggested_features


def plot_correlation_matrix(dataset, showtext=True):
    if not type(dataset) == pd.core.frame.DataFrame:
        print('Only works with Pandas for now.')
        return
    data = dataset.corr()
    fig, ax = plt.subplots()
    ax.imshow(data, cmap='cool')

    if showtext:
        ax.set_xticks(np.arange(len(data.columns)))
        ax.set_yticks(np.arange(len(data.columns)))
        ax.set_xticklabels(data.columns)
        ax.set_yticklabels(data.columns)

        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        for i in range(len(data.columns)):
           for j in range(len(data.columns)):
               ax.text(j, i, f'{data.iloc[i, j]:.2f}', ha="center", va="center", color="black", fontsize=5)
    ax.set_title("Linear Correlation of Features")
    fig.tight_layout()
    plt.show()


def plot_standard_deviation(feature):
    if len(feature.shape) != 1:
        print('This function only analyses one feature, not a whole dataset.')
        return
    if type(feature) == pd.core.series.Series:
        print('The feature will be converted to an ndarray.')
        feature = feature.to_numpy()
    fig = plt.figure()
    sigma = np.std(feature, axis=0)
    xbar = np.mean(feature)
    xmax = np.max(feature, axis=0)
    if sigma != 1 and xbar != 0:
        print('This feature has not been standardized. Re-Running function with standardization.')
        plot_standard_deviation(standardize_x(feature))
        return
    ax1 = fig.add_subplot(1, 1, 1)
    ax2 = ax1.twinx()
    ax1.hist(feature, bins=50)
    x = np.linspace(xbar-sigma, xmax, 10000)
    y = np.exp(-0.5 * ((x-xbar)/sigma)**2) / np.sqrt(2*np.pi) / sigma
    ax2.plot(x, y, lw=2)

    x = np.linspace(xbar-sigma, xbar+sigma, 10000)
    y = np.exp(-0.5 * ((x-xbar)/sigma)**2) / np.sqrt(2*np.pi) / sigma
    ax2.fill_between(x, y, 0, alpha=0.3, color='r')
    ax2.set_ylim(0)
    plt.legend()
    plt.show()


if __name__ == '__main__':  # for debugging
    # debug plot_standard_deviation
    df = pd.read_csv('./../problems/nations.csv')
    dataset = df.drop(columns=['iso3c', 'country', 'region', 'income'])
    dataset = dataset.to_numpy()
    analyze_null(df)
    analyze_null(dataset)
    df2011 = df[df.year == 2011].drop(columns='year')
    df2011num = df2011.select_dtypes('number')
    pop2011 = df2011num.population
    plot_standard_deviation(pop2011)
    """
    # debug calc_conditional_entropy
    bicycle_dataset = np.loadtxt("./../problems/bicycle_decision_simple.csv", delimiter=",", skiprows=1)
    x = bicycle_dataset[:, 0:4]
    y = bicycle_dataset[:, 4]

    for i in range(4):
        H = calc_conditional_entropy(y, x, i)
        print(H)
    """
    # debug plot_correlation_matrix
    df = pd.read_csv('./../problems/bike_sharing_dataset.csv')
    df = df[['temp', 'atemp', 'windspeed', 'hr', 'holiday']]
    plot_correlation_matrix(df)
