import numpy as np

np.random.seed(42)


class FuzzyCMeans:
    def __init__(self, noOfClusters, norm_order):
        self.noOfClusters = noOfClusters
        self.p = norm_order
        self.fitHistory = []
        self.centers = None

    def _compute_distances(self, x, centers):
        distances = (np.sum(np.abs(x - centers[0, :]) ** self.p, axis=1)) ** (1 / self.p)
        for j in range(1, self.noOfClusters):
            distances_to_add = (np.sum(np.abs(x - centers[j, :]) ** self.p, axis=1)) ** (1 / self.p)
            distances = np.vstack((distances, distances_to_add))
        return distances

    def fit(self, x, max_iterations, epsilon):
        x_min = x.min(axis=0)
        x_max = x.max(axis=0)
        new_centers = np.random.rand(self.noOfClusters, x.shape[1]) * (x_max - x_min) + x_min
        old_centers = new_centers + 1
        count = 0
        weights = None
        while np.sum(np.abs(old_centers - new_centers)) > epsilon and count < max_iterations:
            count += 1
            old_centers = np.copy(new_centers)
            self.fitHistory.append(new_centers.copy())

            distances = self._compute_distances(x, new_centers)
            d2 = distances ** 2
            d2_sum = np.sum(1 / d2, axis=0)
            weights = d2 * d2_sum
            weights = 1 / weights
            weights_sum = np.sum(weights ** 2, axis=1)
            omega = ((weights ** 2).T / weights_sum).T
            new_centers = omega @ x

        self.fitHistory.append(new_centers.copy())
        self.centers = new_centers
        return new_centers, weights.T

    def predict(self, x):
        distances = self._compute_distances(x, self.centers)
        return distances.argmin(axis=0)  # index of cluster