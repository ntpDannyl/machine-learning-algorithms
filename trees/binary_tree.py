import matplotlib.pyplot as plt
import numpy as np
import time


class BinaryTree:
    def __init__(self, var_no, value, operator):
        self.root_node = TreeNode(0, value, operator=operator, var_no=var_no)
        self.nodes = []
        self.nodes.append(self.root_node)
        self.leaf_nodes = []
        self.leaf_nodes.append(0)

    def add_node(self, child_of, branch, value, operator='<', var_no=0):
        node = TreeNode(len(self.nodes), value, child_of=child_of, operator=operator, var_no=var_no)
        self.leaf_nodes.append(node.number)
        self.nodes.append(node)

        parent = self.nodes[child_of]
        if branch is True:
            parent.left_true = node
        else:
            parent.right_false = node

        if parent.left_true is not None and parent.right_false is not None:
            to_delete = self.leaf_nodes.index(parent.number)
            del self.leaf_nodes[to_delete]
        return node.number

    def trace(self, x):
        return self.root_node.trace(x)[0]

    def eval(self, x):
        trace_route = self.trace(x)
        y = np.zeros(len(trace_route))
        for i in range(len(y)):
            y[i] = self.nodes[trace_route[i][-1]]()
        return y

    def weighted_path_length(self, X):
        traceroute = self.trace(X)
        sum = 0
        for i in range(len(traceroute)):
            sum += len(traceroute[i]) - 1
        return sum

    def number_of_leafs(self):
        return len(self.leaf_nodes)

    def print(self, maxlevels=-1):
        ongoingstring = "\\node {"+self.root_node.condition_string()+" }\n"
        if self.root_node.left_true is not None:
            ongoingstring = self.root_node.left_true.add_my_string(ongoingstring, maxlevels, '  ')
        if self.root_node.right_false is not None:
            ongoingstring = self.root_node.right_false.add_my_string(ongoingstring, maxlevels, '  ')
        ongoingstring = ongoingstring + " ;"
        return ongoingstring


class TreeNode:
    def __init__(self, number, value, child_of=None, operator='<', var_no=0):
        self.number = number
        self.child_of = child_of
        self.left_true = None
        self.right_false = None
        self.value = value
        self.var_no = var_no
        self.operator = operator

    def __call__(self):
        return self.value

    def is_leaf_node(self):
        if self.left_true is not None and self.right_false is not None:
            return False
        else:
            return True

    def eval_condition(self, x):
        condition = None
        if self.operator == '=':
            condition = x[:, self.var_no] == self.value
        elif self.operator == '<':
            condition = x[:, self.var_no] < self.value
        elif self.operator == '<':
            condition = x[:, self.var_no] > self.value
        else:
            print('Wrong operator in TreeNode.eval_condition')
        return condition

    def trace(self, x, index=None, trace_route=None):
        if index is None:
            index = np.arange(len(x))
        if trace_route is None:
            trace_route = [[] for x in range(len(x))]

        for k in index:
            trace_route[k].append(self.number)

        if self.is_leaf_node():
            return trace_route, index

        cond = self.eval_condition(x[index])
        true_index = index[cond]
        false_index = index[~cond]

        if self.left_true is not None and true_index.size != 0:
            trace_route = self.left_true.trace(x, true_index, trace_route)[0]
        if self.right_false is not None and false_index.size != 0:
            trace_route = self.right_false.trace(x, false_index, trace_route)[0]
        return trace_route, index

    def condition_string(self):
        if not self.is_leaf_node():
            mystring = "$\\tiny %d \\mathrel{||} x[%d] %s %.2f$" % (self.number, self.var_no, self.operator, self.value)
        else:
            mystring = "$\\tiny %d \\mathrel{||} %.2f$" % (self.number, self.value)
        return mystring

    def add_my_string(self, ongoingstring, levelsleft=-1, indent=''):
        if levelsleft == 0:
            return ongoingstring
        if not self.is_leaf_node():
            ongoingstring = ongoingstring + indent + "child { node {" + self.condition_string() + " }\n"
        else:
            ongoingstring = ongoingstring + indent + "child { node[fill=gray!30] {" + self.condition_string() + " }\n"
        if self.left_true is not None:
            ongoingstring = self.left_true.add_my_string(ongoingstring, levelsleft - 1, indent + '  ')
        if self.right_false is not None:
            ongoingstring = self.right_false.add_my_string(ongoingstring, levelsleft - 1, indent + '  ')
        ongoingstring = ongoingstring + indent + "}\n"

        return ongoingstring


if __name__ == '__main__':
    np.random.seed(3)

    bicycle_tree = BinaryTree(0, 1, '=')
    node_number = bicycle_tree.add_node(0, False, 1, operator='=', var_no=1)
    bicycle_tree.add_node(node_number, False, 0)
    bicycle_tree.add_node(node_number, True, 1)
    node_number = bicycle_tree.add_node(0, True, 1, operator='=', var_no=2)
    bicycle_tree.add_node(node_number, True, 0)
    node_number = bicycle_tree.add_node(node_number, False, 1, operator='=', var_no=3)
    bicycle_tree.add_node(node_number, True, 0)
    bicycle_tree.add_node(node_number, False, 1)

    '''  This is our tree to decide, whether to ride a bike:
    
                Weather good?
               -             - 
              y               n
             -                 -
            Snow?              Car broken?
           -     -             -           -
          -       -           y             n
         y         n         -               -
        -           -       1                 0
       -             -      
      0              start at 8?
                    -           -
                   y             n
                  -               -
                 0                 1 
    '''

    x = np.array([True, False, False, False]).reshape(1, 4)
    print(bicycle_tree.eval(x))  # = y
    print(bicycle_tree.trace(x))  # = trace_route
    print(bicycle_tree.print())
    # Testing with an array of inputs
    x = np.random.randint(2, size=(1000000, 4))

    # Time a lot of Queries.
    t1 = time.time()
    _ = bicycle_tree.eval(x)
    t2 = time.time()
    print(t2 - t1)
