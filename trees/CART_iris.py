import numpy as np
from CART import CART
from basics.refactor import train_test_split

if __name__ == '__main__':
    np.random.seed(42)
    dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = dataset[:, 0:4]
    y = dataset[:, 4]
    x_train, x_test, y_train, y_test = train_test_split(x, y, 0.8)
    myTree = CART(min_leaf_node_size=5)
    myTree.fit(x_train, y_train)

    y_predict = myTree.predict(x_test)
    diffs = y_predict - y_test
    print(f'Accuracy: {len(diffs[diffs == 0]) / len(diffs):.2f}%')

    # A plot like in Frochte 152 would be nice.
