import copy
import numpy as np
import matplotlib.pyplot as plt


def heaviside(x):
    y = np.ones_like(x, dtype=float)
    y[x <= 0] = 0
    return y


def tanh(x):
    y = (np.exp(x)-np.exp(-x)) / (np.exp(x)+np.exp(-x))
    return y


def sigmoid(x):
    y = 1/(1+np.exp(-x))
    return y


def linear(x):
    y = copy.deepcopy(x)
    return y


def ReLU(x):
    y = copy.deepcopy(x)
    y[x <= 0] = 0.0
    return y


def ELU(x):
    y = copy.deepcopy(x)
    y[x <= 0] = np.exp(x[x <= 0]) - 1
    return y


if __name__ == '__main__':
    fig = plt.figure(figsize=(13, 8))
    x = np.linspace(-2, 2, 2000)

    y = heaviside(x)
    ax = fig.add_subplot(231)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("heaviside")

    y = tanh(5*x)
    ax = fig.add_subplot(232)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("tanh(5x)")

    y = sigmoid(5*x)
    ax = fig.add_subplot(233)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("sigmoid(5x)")

    y = linear(x)
    ax = fig.add_subplot(234)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("linear")

    y = ReLU(x)
    ax = fig.add_subplot(235)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("ReLU")

    y = ELU(x)
    ax = fig.add_subplot(236)
    ax.plot(x, y, 'k')
    ax.set_ylim([-1.25, 1.25])
    ax.set_xlim([-2, 2])
    ax.grid(True)
    ax.set_title("ELU")
    # plt.savefig("activation_functions.png", dpi=1000, bbox_inches='tight', pad_inches=0.5)
    plt.show()
