import numpy as np
import matplotlib.pyplot as plt
from basics.refactor import standardize_x

np.random.seed(42)


class PCA:
    def __init__(self, x):
        self.x = x
        self.eigenvalues, self.eigenvectors = np.linalg.eig(np.cov(self.x.T))
        self.eigenvalues = np.sort(self.eigenvalues)[::-1]
        self.eigenvalues = self.eigenvalues / np.sum(self.eigenvalues)

    def get_projection(self, number_of_components):
        #eigenvalues_index = np.argsort(self.eigenvalues)[::-1]
        w_projected = self.eigenvectors[:, 0:number_of_components]
        x_projected = (w_projected.T @ self.x.T).T
        return x_projected, w_projected

    def plot(self):
        cumulative_eigenvalues = np.cumsum(self.eigenvalues)
        plt.figure()
        plt.bar(range(1, len(self.eigenvalues) + 1), self.eigenvalues, alpha=0.25, align='center',
                label='Proportion of Variance', color='gray')
        plt.step(range(1, len(self.eigenvalues) + 1), cumulative_eigenvalues, where='mid',
                 label='Cumulative Proportion of Variance', c='k')
        plt.xlabel('Principal Components')
        plt.ylabel('Proportion (%)')
        plt.xticks(np.arange(1, len(self.eigenvalues) + 1, step=1))
        plt.legend(loc='center right')
        plt.show()


if __name__ == '__main__':
    # this example shows how the PCA works in theory without using the PCA class above
    xs = np.arange(0.2, 0.8, 0.0025)
    ys = np.arange(0.2, 0.8, 0.0025)
    zs = np.arange(0.2, 0.8, 0.0025)

    dx = 0.15 * (np.random.rand(xs.shape[0]) - 0.5)
    dy = 0.30 * (np.random.rand(ys.shape[0]) - 0.5)
    dz = 0.20 * (np.random.rand(zs.shape[0]) - 0.5)

    x = 0.5 * xs + 0.25 * ys + 0.3 * zs + dx
    y = 0.3 * xs + 0.45 * ys + 0.3 * zs + dy
    z = 0.1 * xs + 0.30 * ys + 0.6 * zs + dz
    dataset = np.vstack((x, y, z)).T

    X = standardize_x(dataset)

    sigma = np.cov(X.T)
    (eigenvalues, eigenvectors) = np.linalg.eig(sigma)

    fig = plt.figure(2)
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    ax.scatter(x, y, z, c='red', s=60, alpha=0.3)
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_zlim([0, 1])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    x_m = np.array([x[0], x[0], x[0]])
    y_m = np.array([x[1], x[1], x[1]])
    z_m = np.array([x[2], x[2], x[2]])
    d = np.zeros_like(eigenvectors)
    d[:, 0] = eigenvalues[0] / 4 * eigenvectors[:, 0]
    d[:, 1] = eigenvalues[1] / 4 * eigenvectors[:, 1]
    d[:, 2] = eigenvalues[2] / 4 * eigenvectors[:, 2]
    ax.quiver(x_m, y_m, z_m, d[0, :], d[1, :], d[2, :])
    print(np.std((eigenvectors[:, 0].T @ X.T).T))
    plt.show()
