import numpy as np
import matplotlib.pyplot as plt


def two_moons_problem(samples=240, noise=0):
    t_moon0 = np.linspace(0, np.pi, samples)
    t_moon1 = np.linspace(0, np.pi, samples)
    moon0x = np.cos(t_moon0)
    moon0y = np.sin(t_moon0)
    moon1x = 1 - np.cos(t_moon1)
    moon1y = 0.5 - np.sin(t_moon1)

    x = np.vstack((np.append(moon0x, moon1x), np.append(moon0y, moon1y))).T
    x = x + noise / 100 * np.random.normal(size=x.shape)
    y = np.hstack([np.zeros(samples), np.ones(samples)])

    return x, y


if __name__ == '__main__':
    (x, y) = two_moons_problem()

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 1, 1)
    indexA = np.flatnonzero(y > 0.5)
    indexB = np.flatnonzero(y < 0.5)
    ax.scatter(x[indexA, 0], x[indexA, 1], color='red', marker='o')
    ax.scatter(x[indexB, 0], x[indexB, 1], color='black', marker='+')
    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_ylim([-1, 2])
    ax.set_ylim([-1, 2])
    ax.set_title("Two Moons Set")
    plt.show()
