import numpy as np
from problems.two_moons_problem import two_moons_problem
from sklearn import svm
import matplotlib.pyplot as plt


if __name__ == '__main__':
    x, y = two_moons_problem(240, 2)
    XX, YY = np.mgrid[-1:2:0.01, -1:2:0.01]
    X = np.array([XX.ravel(), YY.ravel()]).T
    indexA = np.flatnonzero(y > 0.5)
    indexB = np.flatnonzero(y < 0.5)

    for kernel in ['poly', 'rbf', 'linear', 'sigmoid']:
        if kernel == 'poly':
            my_svm = svm.SVC(kernel=kernel, decision_function_shape='ovr', degree=7, C=1000)
        else:
            my_svm = svm.SVC(kernel=kernel, decision_function_shape='ovr')
        my_svm.fit(x, y)
        yP = my_svm.predict(X)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        yP = yP.reshape(XX.shape)
        ax.pcolormesh(XX, YY, yP, cmap=plt.cm.Set1, shading='auto')
        ax.scatter(x[indexA, 0], x[indexA, 1], color='white', marker='o')
        ax.scatter(x[indexB, 0], x[indexB, 1], color='black', marker='+')
        ax.set_xlabel('$x_0$')
        ax.set_ylabel('$x_1$')
        ax.set_xlim([-1, 2])
        ax.set_ylim([-1, 2])
        if kernel == 'poly':
            ax.set_title(f'Classification with {kernel}-kernel (degree=7, C=1000)')
        else:
            ax.set_title(f'Classification with {kernel}-kernel')
        plt.show()
