import numpy as np
import matplotlib.pyplot as plt
from problems.two_moons_problem import two_moons_problem

np.random.seed(42)


def bubble_set_normal(mx, my, number, s):
    x = np.random.normal(0, s, number) + mx
    y = np.random.normal(0, s, number) + my
    return x, y


def four_balls(n1, n2, n3, n4):
    dataset = np.zeros((n1 + n2 + n3 + n4, 2))
    dataset[0:n1, 0], dataset[0:n1, 1] = bubble_set_normal(2.5, 1.0, n1, 0.5)
    dataset[n1:n1 + n2, 0], dataset[n1:n1 + n2, 1] = bubble_set_normal(2.0, -3.0, n2, 0.3)
    dataset[n1 + n2:n1 + n2 + n3, 0], dataset[n1 + n2:n1 + n2 + n3, 1] = bubble_set_normal(-2.0, 5.0, n3, 0.6)
    dataset[n1 + n2 + n3:n1 + n2 + n3 + n4, 0], dataset[n1 + n2 + n3:n1 + n2 + n3 + n4, 1] = bubble_set_normal(-4.0, -1.0,  n4, 0.9)
    return dataset


def mouse(n_ear1, n_ear2, n_face):

    dataset = np.zeros((n_ear1 + n_ear2 + n_face, 2))
    dataset[0:n_ear1, 0], dataset[0:n_ear1, 1] = bubble_set_normal(-0.75, 0.75, n_ear1, 0.15)
    dataset[n_ear1:n_ear1 + n_ear2, 0], dataset[n_ear1:n_ear1 + n_ear2, 1] = bubble_set_normal(0.75, 0.75, n_ear2, 0.15)
    dataset[n_ear1 + n_ear2:n_ear1 + n_ear2 + n_face, 0], dataset[n_ear1 + n_ear2:n_ear1 + n_ear2 + n_face, 1] = bubble_set_normal(0, 0, n_face, 0.29)
    return dataset


def two_circles(samples):
    phi = np.linspace(0, 2 * np.pi, samples)
    x1 = 1.5 * np.cos(phi)
    y1 = 1.5 * np.sin(phi)
    x2 = 0.5 * np.cos(phi)
    y2 = 0.5 * np.sin(phi)
    x = np.vstack((np.append(x1, x2), np.append(y1, y2))).T
    x = x + 0.1 * np.random.normal(size=x.shape)
    return x


def two_moons(samples, noise):
    moons, _ = two_moons_problem(samples, noise)  # we don't need y for unsupervised learning
    return moons


def just_noise(samples):
    return np.random.random((samples, 2))


def plot(ax, plt, title):
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.grid(True, linestyle='-', color='0.75')
    ax.set_aspect('equal', 'datalim')
    ax.set_title(title)
    plt.show()


if __name__ == '__main__':
    n1 = n2 = n3 = n4 = 400
    # Example 1
    equal_balls = four_balls(n1, n2, n3, n4)

    fig = plt.figure(1)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(equal_balls[0:n1, 0], equal_balls[0:n1, 1], c='red', s=60, alpha=0.2, marker='*')
    ax.scatter(equal_balls[n1:n1 + n2, 0], equal_balls[n1:n1 + n2, 1], c='blue', s=60, alpha=0.2, marker='s')
    ax.scatter(equal_balls[n1 + n2:n1 + n2 + n3, 0], equal_balls[n1 + n2:n1 + n2 + n3, 1], c='black', s=60, alpha=0.2, marker='<')
    ax.scatter(equal_balls[n1 + n2 + n3:n1 + n2 + n3 + n4, 0], equal_balls[n1 + n2 + n3:n1 + n2 + n3 + n4, 1], c='green', s=60, alpha=0.2, marker='o')
    plot(ax, plt, "Vier gleich grosse Mengen")

    # Example 2
    n1 = n2 = 100
    n3 = n4 = 400

    different_balls = four_balls(n1, n2, n3, n4)

    fig = plt.figure(2)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(different_balls[0:n1, 0], different_balls[0:n1, 1], c='red', s=60, alpha=0.2, marker='*')
    ax.scatter(different_balls[n1:n1 + n2, 0], different_balls[n1:n1 + n2, 1], c='blue', s=60, alpha=0.2, marker='s')
    ax.scatter(different_balls[n1 + n2:n1 + n2 + n3, 0], different_balls[n1 + n2:n1 + n2 + n3, 1], c='black', s=60, alpha=0.2, marker='<')
    ax.scatter(different_balls[n1 + n2 + n3:n1 + n2 + n3 + n4, 0], different_balls[n1 + n2 + n3:n1 + n2 + n3 + n4, 1], c='green', s=60, alpha=0.2, marker='o')
    plot(ax, plt, "Vier unterschiedlich grosse Mengen")


    # Example 3
    n_ear1 = n_ear2 = 150
    n_face = 700
    mouse_set = mouse(n_ear1, n_ear2, n_face)

    fig = plt.figure(3)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(mouse_set[0:n_ear1, 0], mouse_set[0:n_ear1, 1], c='red', s=60, alpha=0.2, marker='*')
    ax.scatter(mouse_set[n_ear1:n_ear1 + n_ear2, 0], mouse_set[n_ear1:n_ear1 + n_ear2, 1], c='blue', s=60, alpha=0.2, marker='s')
    ax.scatter(mouse_set[n_ear1 + n_ear2:n_ear1 + n_ear2 + n_face, 0], mouse_set[n_ear1 + n_ear2:n_ear1 + n_ear2 + n_face, 1], c='black', s=60, alpha=0.2, marker='<')
    plot(ax, plt, "Mouse Set")

    # Example 4
    samples = 240
    noise = 0
    moons = two_moons(samples, noise)

    fig = plt.figure(4)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(moons[0:samples, 0], moons[0:samples, 1], c='red', s=60, alpha=0.2, marker='*')
    ax.scatter(moons[samples:samples*2, 0], moons[samples:samples*2, 1], c='black', s=60, alpha=0.2, marker='<')
    plot(ax, plt, "Two Moons Set")


    # Example 5
    samples = 800
    circles = two_circles(samples)

    fig = plt.figure(5)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(circles[0:samples, 0], circles[0:samples, 1], c='red', s=60, alpha=0.2, marker='*')
    ax.scatter(circles[samples:samples*2, 0], circles[samples:samples*2, 1], c='black', s=60, alpha=0.2, marker='<')
    plot(ax, plt, "Two Circles")

    # Example 6
    samples = 1000
    noise = just_noise(samples)
    fig = plt.figure(6)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(noise[0:samples, 0], noise[0:samples, 1], c='red', s=60, alpha=0.2, marker='*')
    plot(ax, plt, "Noise")