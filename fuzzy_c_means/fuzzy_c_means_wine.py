import numpy as np
from fuzzy_c_means import FuzzyCMeans

np.random.seed(42)
n_clusters = 3
max_iterations = 42
epsilon = 10 ** -3

if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/wine.csv", delimiter=",")

    x = dataset[:, 1:14]
    y = dataset[:, 0] - 1

    x_max = np.max(x, axis=0)
    x_min = np.min(x, axis=0)
    x_hat = (x - x_min) / (x_max - x_min)

    for p in (1, 2, 4):
        fuzzy_c_means = FuzzyCMeans(n_clusters, p)
        _, W = fuzzy_c_means.fit(x, max_iterations, epsilon)

        cluster = np.argmax(W, axis=1)

        index1, count1 = np.unique(cluster[0:59], return_counts=True)
        index2, count2 = np.unique(cluster[59:130], return_counts=True)
        index3, count3 = np.unique(cluster[130:178], return_counts=True)
    
        max1 = index1[np.unravel_index(count1.argmax(), count1.shape)]
        max2 = index2[np.unravel_index(count2.argmax(), count2.shape)]
        max3 = index3[np.unravel_index(count3.argmax(), count3.shape)]

        y[0:59] = max1
        y[59:130] = max2
        y[130:178] = max3

        counter = 0
        for i in range(0, len(y)):
            if int(cluster[i, None]) == int(y[i, None]):
                counter = counter + 1

        print(f'{counter}/{len(y)} correctly clustered with {n_clusters} clusters and {p}-norm: {counter / len(y) * 100:.2f}%')
