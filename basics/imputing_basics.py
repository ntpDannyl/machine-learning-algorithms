import numpy as np
import pandas as pd

from knn.knn import Knn
from basics.refactor import train_test_split, standardize_x
np.random.seed(42)

# this script compares different imputing techniques and is an exercise for working with missing data


if __name__ == '__main__':

    # 0: Create a useful dataset from nations.csv

    df = pd.read_csv('./../problems/nations.csv')
    df.drop(columns='iso3c', inplace=True)

    # We need to replace the string with a numerical value. We use the average of the income-groups for that
    df['income'].replace(to_replace=['Low income'], value=647.5, inplace=True)
    df['income'].replace(to_replace=['Lower middle income'], value=2445.5, inplace=True)
    df['income'].replace(to_replace=['Upper middle income'], value=7975.5, inplace=True)
    df['income'].replace(to_replace=['High income'], value=27218.0, inplace=True)
    df['income'].replace(to_replace=['High income: OECD'], value=42380.0, inplace=True)

    df['region'] = pd.factorize(df.region)[0]  # every region gets it's own number

    # we just keep the rows where population, life expectancy and birthrate are complete
    df = df[df.population.notna() & df.life_expect.notna() & df.birth_rate.notna()]

    # get the ID's of all NaNs in the feature gdp per capita and the neonate mortal rate
    ids_with_nan = df.gdp_percap.isna() | df.neonat_mortal_rate.isna()
    print(f'{ids_with_nan.sum() / df.shape[0] * 100:.2f} % of all entries contain at least one NaN.')
    print(df[ids_with_nan].country.value_counts())  # print out the amount of missing values per country

    df = df[df.gdp_percap.notna() | df.neonat_mortal_rate.notna()]  # if both entries are NaN we don't keep them

    df_numbers_only = df.select_dtypes('number')  # drop all features that aren't numbers. In our case it's "country".
    y = df_numbers_only.life_expect
    x = standardize_x(df_numbers_only.drop(columns=['life_expect']))

    # 1: get the mean average error for a not-imputed dataset, but we drop all rows that contain NaN
    ids_without_nan = df.gdp_percap.notna() & df.neonat_mortal_rate.notna()  # it's the same as ~ids_with_nan
    x_train, x_test, y_train, y_test = train_test_split(x[ids_without_nan], y[ids_without_nan], 0.7, using_pd=True)
    knn = Knn()
    knn.regression_fit(x_train.to_numpy(), y_train.to_numpy())
    y_predict = knn.regression_predict(x_test.to_numpy(), k=2, smear=10 ** -3)
    print(f'MAE on testset with dropped rows in train: {np.mean(np.abs(y_predict - y_test)):.3f}')

    # 2: drop all features containing NaN
    x_dropped = x.drop(columns=['gdp_percap', 'neonat_mortal_rate'])
    x_train_dropped = x_dropped.drop(x_test.index)
    y_train_dropped = y.drop(x_test.index)
    knn_dropped = Knn()
    knn_dropped.regression_fit(x_train_dropped.to_numpy(), y_train_dropped.to_numpy())
    y_predict = knn_dropped.regression_predict(x_test.drop(columns=['gdp_percap', 'neonat_mortal_rate']).to_numpy(), k=2, smear=10 ** -3)
    print(f'MAE on testset with dropped features in train: {np.mean(np.abs(y_predict - y_test)):.3f}')

    # 3: impute Missing data with mean value
    x_train_mean = x.drop(x_test.index).fillna(x.mean())
    y_train_mean = y.drop(x_test.index)
    knn_mean = Knn()
    knn_mean.regression_fit(x_train_mean.to_numpy(), y_train_mean.to_numpy())
    y_predict = knn_mean.regression_predict(x_test, k=2, smear=10 ** -3)
    print(f'MAE on testset with mean-imputer: {np.mean(np.abs(y_predict - y_test)):.3f}')

    # 4: impute Missing data with median value
    x_train_median = x.drop(x_test.index).fillna(x.median())
    y_train_median = y.drop(x_test.index)
    knn_median = Knn()
    knn_median.regression_fit(x_train_median.to_numpy(), y_train_median.to_numpy())
    y_predict = knn_median.regression_predict(x_test, k=2, smear=10 ** -3)
    print(f'MAE on testset with median-imputer: {np.mean(np.abs(y_predict - y_test)):.3f}')

    # 5: impute NaN via KNN-regression

    # 5.1: impute gdp(_percap)
    y_train_gdp = x[ids_without_nan].gdp_percap.copy()
    x_gdp = x.drop(columns=['gdp_percap'])
    knn_gdp = Knn()
    knn_gdp.regression_fit(x_gdp[ids_without_nan].to_numpy(), y_train_gdp.to_numpy())
    nan_ids = x.gdp_percap.isna()
    x.loc[nan_ids, 'gdp_percap'] = knn_gdp.regression_predict(x_gdp[nan_ids].to_numpy(), k=2, smear=10 ** -3)

    # 5.2: impute neonat_mortal_rate (=nmr)
    y_train_nmr = x[ids_without_nan].neonat_mortal_rate.copy()
    x_nmr = x.drop(columns=['neonat_mortal_rate'])
    knn_nmr = Knn()
    knn_nmr.regression_fit(x_nmr[ids_without_nan].to_numpy(), y_train_nmr.to_numpy())
    nan_ids = x.neonat_mortal_rate.isna()
    x.loc[nan_ids, 'neonat_mortal_rate'] = knn_nmr.regression_predict(x_nmr[nan_ids].to_numpy(), k=2, smear=10 ** -3)

    # 5.3 compare with 1.

    # stack the old train_set with new imputed values
    x_train_knn = np.vstack((x[~ids_without_nan].to_numpy(), x_train.to_numpy()))
    y_train_knn = np.hstack((y[~ids_without_nan].to_numpy(), y_train.to_numpy()))

    # train kkn and compare
    knn_knn = Knn()
    knn_knn.regression_fit(x_train_knn, y_train_knn)
    y_predict = knn_knn.regression_predict(x_test, k=2, smear=10 ** -3)
    print(f'MAE on testset with kNN-Imputer: {np.mean(np.abs(y_predict - y_test)):.3f}')

    """
    # 6: Trying to predict missing data with our knn from 1 and imputed data. 
    # This section is not as important as the others
    y_knn = knn.regression_predict(x[~ids_without_nan].to_numpy(), k=2, smear=10 ** -3)
    print(f'MAE for for data with kNN-Imputer: {np.mean(np.abs(y_knn - y[~ids_without_nan])):.3f}')

    y_mean = knn.regression_predict(x_train_mean.drop(x_train.index).to_numpy(), k=2, smear=10 ** -3)
    print(f'MAE for for data with mean-Imputer: {np.mean(np.abs(y_mean - y[~ids_without_nan])):.3f}')

    knn_pure_drop = Knn()
    knn_pure_drop.regression_fit(x_train.drop(columns=['gdp_percap', 'neonat_mortal_rate']).to_numpy(), y_train.to_numpy())
    x_test_nan = x_train_mean.drop(x_train.index)
    x_test_nan = x_test_nan.drop(columns=['gdp_percap', 'neonat_mortal_rate'])
    y_mean = knn_pure_drop.regression_predict(x_test_nan.to_numpy(), k=2, smear=10 ** -3)
    print(f'MAE for data with NaN {np.mean(np.abs(y_mean - y[~ids_without_nan])):.3f}')
    """

