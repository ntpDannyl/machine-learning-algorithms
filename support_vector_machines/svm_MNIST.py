from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
from basics.refactor import normalize_x
import numpy as np


if __name__ == '__main__':
    print('fetching...')
    dataset = np.loadtxt("./../problems/wine.csv", delimiter=",")

    x = dataset[:, 1:14]
    y = dataset[:, 0] - 1

    x_hat = normalize_x(x)

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    svm_classifier = svm.SVC()
    print('fitting...')
    svm_classifier.fit(x_train, y_train)
    print('predicting...')
    prediction = svm_classifier.predict(x_test)
    print(f"Accuracy - {accuracy_score(y_pred=prediction, y_true=y_test):.2f}")
    confusion = confusion_matrix(y_pred=prediction, y_true=y_test)
