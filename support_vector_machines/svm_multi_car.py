import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from basics.refactor import normalize_x

if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/Cars4Classes.csv", delimiter=",")
    y_train = dataset[:, 0]
    x = normalize_x(dataset[:, 1:3])

    svmRBF = svm.SVC(kernel='rbf', decision_function_shape='ovr', gamma=1)
    svmRBF.fit(x, y_train)

    XX, YY = np.mgrid[0:1:0.01, 0:1:0.01]
    X = np.array([XX.ravel(), YY.ravel()]).T
    yP = svmRBF.predict(X)

    indexA = np.flatnonzero(y_train == 0)
    indexB = np.flatnonzero(y_train == 1)
    indexC = np.flatnonzero(y_train == 2)
    indexD = np.flatnonzero(y_train == 3)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    yP = yP.reshape(XX.shape)
    ax.pcolormesh(XX, YY, yP, cmap=plt.cm.tab20, shading='auto')
    ax.scatter(x[indexA, 0], x[indexA, 1], color='black', marker='o')
    ax.scatter(x[indexB, 0], x[indexB, 1], color='black', marker='x')
    ax.scatter(x[indexC, 0], x[indexC, 1], color='black', marker='+')
    ax.scatter(x[indexD, 0], x[indexD, 1], color='black', marker='*')
    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_title("Klassifikation mit RBF ($\gamma=1$)")
    plt.show()
