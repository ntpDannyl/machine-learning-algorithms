import numpy as np
import matplotlib.pyplot as plt
from problems.two_moons_problem import two_moons_problem
from knn import Knn

# PARAMS
np.random.seed(42)
train_percentage = 0.8
norm_order = 1


if __name__ == '__main__':
    x_train, y_train = two_moons_problem()

    XX, YY = np.mgrid[-1:2:0.01, -1:2:0.01]
    X = np.array([XX.ravel(), YY.ravel()]).T
    yP = np.zeros(X.shape[0])

    for i in range(X.shape[0]):
        yP[i] = Knn.knn_classification(x_train, y_train, X[i, :], 3, norm_order=norm_order)

    index_a = np.flatnonzero(y_train > 0.5)
    index_b = np.flatnonzero(y_train < 0.5)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    yP = yP.reshape(XX.shape)
    ax.pcolormesh(XX, YY, yP, cmap=plt.cm.Set1, shading='auto')
    ax.scatter(x_train[index_a, 0], x_train[index_a, 1], color='white', marker='o')
    ax.scatter(x_train[index_b, 0], x_train[index_b, 1], color='black', marker='+')
    ax.set_xlabel('$x_0$')
    ax.set_ylabel('$x_1$')
    ax.set_xlim([-1, 2])
    ax.set_ylim([-1, 2])
    ax.set_title(f'Classification with {norm_order}-norm')
    plt.show()
