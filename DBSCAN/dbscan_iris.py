import numpy as np
from dbscan import DBSCAN
from basics.refactor import normalize_x

np.random.seed(42)
min_points = 5
normalize = True


if __name__ == '__main__':
    iris_dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = iris_dataset[:, 0:4]
    y = iris_dataset[:, 4] - 1  # we want to start counting at 0
    if normalize:
        x = normalize_x(x)

    sets_and_clusters = [
        {
            'dataset': x[:, [0, 1]],
            'epsilon': 0.1
        },
        {
            'dataset': x[:, [2, 3]],
            'epsilon': 0.1
        },
        {
            'dataset': x[:, [0, 2]],
            'epsilon': 0.1
        },
        {
            'dataset': x[:, [1, 3]],
            'epsilon': 0.1
        }
    ]

    for entry in sets_and_clusters:  # all possible 2D-Combinations
        x = entry['dataset']
        epsilon = entry['epsilon']
        dbscan = DBSCAN(x)
        _ = dbscan.analyse_epsilon(min_points)
        c = dbscan.fit_predict(epsilon, min_points)
        dbscan.plot()

    # 4D
    x = iris_dataset[:, 0:4]
    epsilon = 0.5
    dbscan = DBSCAN(x)
    _ = dbscan.analyse_epsilon(min_points)
    c = dbscan.fit_predict(epsilon, min_points)
    dbscan.plot('4D DBSCAN with 2D Plot')
