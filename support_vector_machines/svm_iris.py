import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from sklearn import svm
from basics.refactor import normalize_x
matplotlib.use('TkAgg')

if __name__ == '__main__':
    dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    yTrain = dataset[:, 4] - 1  # The iris y start at 1, not at 0.

    features = [
        {
            'x': [0, 1],
            'x_label': 'Kelchblattlaenge (cm)',
            'y_label': 'Kelchblattbreite (cm)'
        },
        {
            'x': [2, 3],
            'x_label': 'Kronblattlaenge (cm)',
            'y_label': 'Kronblattbreite (cm)'
        },
        {
            'x': [0, 2],
            'x_label': 'Kelchblattlaenge (cm)',
            'y_label': 'Kronblattlaenge (cm)'
        },
        {
            'x': [1, 3],
            'x_label': 'Kelchblattbreite (cm)',
            'y_label': 'Kronblattbreite (cm)'
        },
    ]

    for feature in features:
        x = normalize_x(dataset[:, feature['x']])

        svmRBF = svm.SVC(kernel='rbf', decision_function_shape='ovr', gamma=1)
        svmRBF.fit(x, yTrain)

        XX, YY = np.mgrid[0:1:0.01, 0:1:0.01]
        X = np.array([XX.ravel(), YY.ravel()]).T
        yP = svmRBF.predict(X)

        indexA = np.flatnonzero(yTrain == 0)
        indexB = np.flatnonzero(yTrain == 1)
        indexC = np.flatnonzero(yTrain == 2)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        yP = yP.reshape(XX.shape)
        ax.pcolormesh(XX, YY, yP, cmap=plt.cm.tab20, shading='auto', antialiased=True)
        ax.scatter(x[indexA, 0], x[indexA, 1], color='black', marker='o')
        ax.scatter(x[indexB, 0], x[indexB, 1], color='black', marker='x')
        ax.scatter(x[indexC, 0], x[indexC, 1], color='black', marker='+')
        ax.set_xlabel(feature['x_label'])
        ax.set_ylabel(feature['y_label'])
        ax.set_xlim([0, 1])
        ax.set_ylim([0, 1])
        ax.set_title("Klassifikation mit RBF ($\gamma=1$)")

    x = normalize_x(dataset[:, [1, 2, 3]])

    svmRBF = svm.SVC(kernel='rbf', decision_function_shape='ovr', gamma=1)
    svmRBF.fit(x, yTrain)

    XX, YY, ZZ = np.mgrid[0:1:0.05, 0:1:0.05, 0:1:0.05]
    X = np.array([XX.ravel(), YY.ravel(), ZZ.ravel()]).T
    yP = svmRBF.predict(X)

    indexA = np.flatnonzero(yTrain == 0)
    indexB = np.flatnonzero(yTrain == 1)
    indexC = np.flatnonzero(yTrain == 2)


    index2A = np.flatnonzero(yP == 0)
    index2B = np.flatnonzero(yP == 1)
    index2C = np.flatnonzero(yP == 2)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    yP = yP.reshape(XX.shape)
    ax.scatter(X[index2A, 0], X[index2A, 1], X[index2A, 2], color='red', alpha=0.1)
    ax.scatter(X[index2B, 0], X[index2B, 1], X[index2B, 2], color='blue', alpha=0.1)
    ax.scatter(X[index2C, 0], X[index2C, 1], X[index2C, 2], color='green', alpha=0.1)
    ax.scatter(x[indexA, 0], x[indexA, 1], x[indexA, 2], color='black', marker='o')
    ax.scatter(x[indexB, 0], x[indexB, 1], x[indexB, 2], color='black', marker='x')
    ax.scatter(x[indexC, 0], x[indexC, 1], x[indexC, 2], color='black', marker='+')
    ax.set_xlabel('Kelchblattbreite (cm)')
    ax.set_ylabel('Kronblattlaenge (cm)')
    ax.set_zlabel('Kronblattbreite (cm)')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_zlim([0, 1])
    ax.set_title("Klassifikation mit RBF ($\gamma=1$)")
    plt.show(block=True)
