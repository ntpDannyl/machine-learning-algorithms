import pandas as pd
from sklearn.feature_selection import f_regression
import numpy as np

def mrmr(X, y, k):  # features:DataFrame, target:Series, number_of_selected_features:int

    # compute F-statistics and initialize correlation matrix
    F = pd.Series(f_regression(X, y)[0], index=X.columns)
    corr = pd.DataFrame(.00001, index=X.columns, columns=X.columns)

    # initialize list of selected features and list of excluded features
    selected = []
    not_selected = X.columns.to_list()

    # repeat K times
    for i in range(k):

        # compute (absolute) correlations between the last selected feature and all the (currently) excluded features
        if i > 0:
            last_selected = selected[-1]
            corr.loc[not_selected, last_selected] = X[not_selected].corrwith(X[last_selected]).abs().clip(.00001)

        # compute FCQ score for all the (currently) excluded features
        scores = F.loc[not_selected] / corr.loc[not_selected, selected].mean(axis=1).fillna(.00001)

        # find best feature, add it to selected and remove it from not_selected
        best = scores.index[scores.argmax()]
        selected.append(best)
        not_selected.remove(best)

    return selected


if __name__ == '__main__':
    from sklearn.datasets import make_classification

    # create some data
    X, y = make_classification(n_samples=1000, n_features=50, n_informative=10, n_redundant=40)
    X = pd.DataFrame(X)
    y = pd.Series(y)
    selected_features = mrmr(X, y, 10)
