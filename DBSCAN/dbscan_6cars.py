import numpy as np
from dbscan import DBSCAN
from basics.refactor import normalize_x, standardize_x

standardize = False
normalize = True
min_points = 5
epsilon = 0.2

if __name__ == '__main__':

    dataset = np.loadtxt('./../problems/Cars6Classes.csv', delimiter=",", skiprows=5, usecols=range(2, 9))
    dataset = dataset[dataset[:, -1].argsort()]

    x = dataset[:, 0:-1]
    y = dataset[:, -1] - 1  # we want to start counting at 0

    if standardize:
        x = standardize_x(x)

    if normalize:
        x = normalize_x(x)

    dbscan = DBSCAN(x)
    _ = dbscan.analyse_epsilon(min_points)
    c = dbscan.fit_predict(epsilon, min_points)

    fp = 0
    labels, label_start, _ = np.unique(y, return_counts=True, return_index=True)
    group_samples = np.zeros(6)
    label_start = np.append(label_start, len(y))
    for i in range(0, 6):
        group_samples[i] = label_start[i + 1] - label_start[i]
    for i in range(0, 6):
        group, count = np.unique(c[label_start[i]:label_start[i + 1]], return_counts=True)
        countSortInd = np.argsort(-count)
        groupValue = group[countSortInd[0]]
        fp = fp + count[countSortInd[0]]
        print(f'Class {i} (clustered as {groupValue}): {count[countSortInd[0]]}/{group_samples[i]:.0f} correct. ({count[countSortInd[0]] / group_samples[i] * 100:.2f}%)')
    print(f'Clustered correctly altogether: {fp / len(y) * 100:.2f}%')
    dbscan.plot()
