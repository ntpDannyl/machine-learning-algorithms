import numpy as np
import matplotlib.pyplot as plt
from CART import CART
from basics.refactor import train_test_split
np.random.seed(42)


if __name__ == '__main__':
    dataset = np.loadtxt('./../problems/bike_sharing_dataset.csv', delimiter=',', skiprows=1)

    x = dataset[:, 0:13]
    y = dataset[:, 15]

    # x = np.delete(x,6, axis=1)

    index = np.flatnonzero(x[:, 8] == 4)
    x = np.delete(x, index, axis=0)
    y = np.delete(y, index, axis=0)

    x_train, x_test, y_train, y_test = train_test_split(x, y, 0.8)

    myTree = CART(min_leaf_node_size=15, threshold=2, is_regression=True)
    myTree.fit(x_train, y_train)
    y_predict = np.round(myTree.predict(x_test))

    plt.figure(1)
    yDiff = y_predict - y_test
    plt.hist(yDiff, 22)
    plt.xlim(-200, 200)
    plt.title('Error on Testdata')
    plt.show()

    plt.figure(2)
    plt.hist(y_test, 22)
    plt.title('Testdata')
    plt.show()
    print(f'Average Deviation: {(np.mean(np.abs(yDiff)))}')
