import numpy as np

np.random.seed(42)

t = 0
t_max = 100000
eta = 0.25
convergence = 1


def heaviside(x):
    y = np.ones_like(x, dtype=float)
    y[x <= 0] = 0
    return y


def predict(x, w):
    x_c = np.ones((x.shape[0], 4))
    x_c[:, 0:3] = x
    y = w @ x_c.T
    y[y > 0] = 1
    y[y <= 0] = 0
    return y


if __name__ == '__main__':
    xO = np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 0]])
    y = np.array([0, 1, 1, 0])
    x = np.ones((len(y), 4))
    x[:, 0:3] = xO
    delta_w = np.zeros(4)
    w = np.random.rand(4) - 0.5
    while (convergence > 0) and (t < t_max):
        t = t + 1
        random_sample = np.random.randint(len(y))
        xB = x[random_sample, :].T
        yB = y[random_sample]
        error = yB - heaviside(w @ xB)
        for j in range(len(xB)):
            delta_w[j] = eta * error * xB[j]
            w[j] = w[j] + delta_w[j]
        convergence = np.linalg.norm(y - heaviside(w @ x.T))

    yPredict = predict(xO, w)
    print(yPredict)
