import numpy as np
import matplotlib.pyplot as plt
from fuzzy_c_means import FuzzyCMeans
from basics.refactor import normalize_x

np.random.seed(42)

epsilon = 10 ** -3
norm_order = 2
max_iterations = 42
normalize = True


if __name__ == '__main__':
    iris_dataset = np.loadtxt("./../problems/iris.csv", delimiter=",")
    x = iris_dataset[:, 0:4]
    y = iris_dataset[:, 4] - 1  # we want to start counting at 0

    if normalize:
        x = normalize_x(x)

    sets_and_clusters = [
        {
            'dataset': x[:, [0, 1]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [2, 3]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [0, 2]],
            'n_clusters': 3
        },
        {
            'dataset': x[:, [1, 3]],
            'n_clusters': 3
        }
    ]

    for entry in sets_and_clusters:  # all possible 2D-Combinations
        dataset = entry['dataset']
        n_clusters = entry['n_clusters']
        fuzzy_c_means = FuzzyCMeans(n_clusters, norm_order)
        _, weights = fuzzy_c_means.fit(dataset, max_iterations, epsilon)

        cluster = np.argmax(weights, axis=1)
        for i in range(n_clusters):
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.scatter(dataset[:, 0], dataset[:, 1], c=weights[:, i], s=60, cmap='binary')
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.grid(True, linestyle='-', color='0.75')
            ax.set_aspect('equal', 'datalim')
            plt.show()

    # 4D
    n_clusters = 3
    fuzzy_c_means = FuzzyCMeans(n_clusters, norm_order)
    _, W = fuzzy_c_means.fit(x, max_iterations, epsilon)

    cluster = np.argmax(W, axis=1)

    index1, count1 = np.unique(cluster[0:50], return_counts=True)
    index2, count2 = np.unique(cluster[50:100], return_counts=True)
    index3, count3 = np.unique(cluster[100:150], return_counts=True)

    max1 = index1[np.unravel_index(count1.argmax(), count1.shape)]
    max2 = index2[np.unravel_index(count2.argmax(), count2.shape)]
    max3 = index3[np.unravel_index(count3.argmax(), count3.shape)]

    y[0:50] = max1
    y[50:100] = max2
    y[100:150] = max3

    counter = 0
    for i in range(0, len(y)):
        if int(cluster[i, None]) == int(y[i, None]):
            counter = counter + 1

    print(f'{counter}/{len(y)} correctly clustered with {n_clusters} clusters and {norm_order}-norm: {counter / len(y) * 100:.2f}%')